#ifndef DEBUGDRAWER_H
#define DEBUGDRAWER_H

class DebugDrawer : public btIDebugDraw
{
public:
	// Horrid hack because btVector3 is __declspec(align(16)) and std::vector<> can't deal with that in 32-bit on Windows.
	struct Vec3
	{
		float x, y, z;

		Vec3(){}
		Vec3(const btVector3& v) : x(v.x()), y(v.y()), z(v.z()) {}

		operator btVector3() const { return btVector3(x, y, z); }
	};

	DebugDrawer();

	virtual void			drawLine(const btVector3& from, const btVector3& to, const btVector3& color);
	virtual void			drawSphere(const btVector3& /*p*/, btScalar /*radius*/, const btVector3& /*color*/) {}
	virtual void			drawTriangle(const btVector3& a, const btVector3& b, const btVector3& c, const btVector3& color, btScalar alpha);
	virtual void			drawContactPoint(const btVector3& /*PointOnB*/, const btVector3& /*normalOnB*/, btScalar /*distance*/, int /*lifeTime*/, const btVector3& /*color*/) {}
	virtual void			reportErrorWarning(const char* warningString);
	virtual void			draw3dText(const btVector3& /*location*/, const char* /*textString*/) {}
	virtual void			setDebugMode(int debugMode) { m_debugMode = debugMode; }
	virtual int				getDebugMode() const { return m_debugMode; }

	void					Start();
	void					Finish();
	void					DrawSolidBox(const btBoxShape* box, const btTransform& xform);
	void					DrawHull(const btConvexHullShape* hull, const btTransform& xform);

private:
	int						m_debugMode;
	std::vector<Vec3>		m_vtxBuf;
	std::vector<int>		m_idxBuf;
};

#endif
