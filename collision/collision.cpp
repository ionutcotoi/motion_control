#include "CollisionPCH.h"
#include "hulls.h"
#include "DebugDrawer.h"

#define DEBUG_DRAWING		0

static const btVector3		g_xAxis(1.0f, 0.0f, 0.0f);
static const btVector3		g_yAxis(0.0f, 1.0f, 0.0f);
static const btVector3		g_zAxis(0.0f, 0.0f, 1.0f);
static const float			g_riserWidth = 0.336f;
static const float			g_boneHeights[] = { 0.55f, 0.0f, 0.625f, 0.0f, 0.625f, 0.11f };
static const btVector3		g_boneAxes[] = { g_yAxis, -g_zAxis, -g_zAxis, g_yAxis, -g_zAxis, g_yAxis };
static const btVector3		g_cageSize(0.10f, 0.189f, 0.198f);
static const float			g_camBoxHeight = 0.17f;
static const float			g_cageWallHeight = 0.01f;

// The order of the objects here is:
//        0               1              2            3             4              5            6              7                       8                     9            10
//	ground (plane), platform (box), riser (box), base (hull), shoulder (hull), arm (hull), elbow (hull), forearm (hull), wrist (hull, includes flange), cage (hull), camera + lens (box)
static std::vector<btCollisionObject*>	g_objects;
static btCollisionWorld*				g_world = NULL;

extern void GenerateAllHulls();

#if DEBUG_DRAWING

static DebugDrawer						g_debugDrawer;

static void DebugDrawWorld(unsigned int enableMask)
{
	for(size_t i = 0; i < g_objects.size(); ++i)
	{
		if( (enableMask & (1 << i)) == 0 )
			continue;

		//g_world->debugDrawObject(g_objects[i]->getWorldTransform(), g_objects[i]->getCollisionShape(), btVector3(1.0f, 0.0f, 0.0f));

		const btTransform& xform = g_objects[i]->getWorldTransform();
		btCollisionShape* shape = g_objects[i]->getCollisionShape();
		int type = shape->getShapeType();
		switch(type)
		{
			case BOX_SHAPE_PROXYTYPE:
			{
				btBoxShape* box = (btBoxShape*)shape;
				g_debugDrawer.DrawSolidBox(box, xform);
				break;
			}

			case CONVEX_HULL_SHAPE_PROXYTYPE:
			{
				btConvexHullShape* hull = (btConvexHullShape*)shape;
				g_debugDrawer.DrawHull(hull, xform);
				break;
			}

		default:
			break;
		}
	}

	//g_world->debugDrawWorld();
}
#endif

static void AddCollsionObject(btCollisionShape* shape)
{
	shape->setMargin(0.0f);
	btCollisionObject* obj = new btCollisionObject;
	obj->setCollisionShape(shape);
	g_objects.push_back(obj);
	g_world->addCollisionObject(obj);
}

extern "C" DLLEXPORT int CollisionInit()
{
	//GenerateAllHulls();

	btDefaultCollisionConfiguration* collisionConfiguration = new btDefaultCollisionConfiguration();
	btCollisionDispatcher* dispatcher = new btCollisionDispatcher(collisionConfiguration);
	btVector3 worldAabbMin(-100.0f, -100.0f, -100.0f);
	btVector3 worldAabbMax( 100.0f,  100.0f,  100.0f);

	btAxisSweep3* broadphase = new btAxisSweep3(worldAabbMin, worldAabbMax);

	g_world = new btCollisionWorld(dispatcher, broadphase, collisionConfiguration);

#if DEBUG_DRAWING
	g_world->setDebugDrawer(&g_debugDrawer);
#endif

	g_objects.reserve(g_numHulls * 2);

	// Add the ground.
	btCollisionShape* groundShape = new btStaticPlaneShape(btVector3(0.0f, 1.0f, 0.0f), 0);
	AddCollsionObject(groundShape);

	// Add boxes for riser and platform. They will be unit cubes for now and will
	// be set to the correct size in the CollisionCheck() function.
	btBoxShape* box = new btBoxShape(btVector3(0.5f, 0.5f, 0.5f));
	AddCollsionObject(box);
	box = new btBoxShape(btVector3(0.5f, 0.5f, 0.5f));
	AddCollsionObject(box);

	// Add the hulls for the robot and cage.
	for(int hullIdx = 0; hullIdx < g_numHulls; ++hullIdx)
	{
		btConvexHullShape* shape = new btConvexHullShape();
		const float* p = g_hullInfo[hullIdx].points;
		for(int vertIdx = 0; vertIdx < g_hullInfo[hullIdx].numPoints; ++vertIdx, p += 3)
			shape->addPoint(btVector3(p[0], p[1], p[2]), false);
		shape->recalcLocalAabb();
		shape->initializePolyhedralFeatures();

		AddCollsionObject(shape);
	}

	// Add the camera box.
	box = new btBoxShape(btVector3(0.5f, 0.5f, 0.5f));
	AddCollsionObject(box);

	return 1;
}

extern "C" DLLEXPORT void CollisionShutdown()
{
	for(size_t i = 0; i < g_objects.size(); ++i)
		delete g_objects[i];

	g_objects.clear();
	delete g_world;
	g_world = NULL;
}

class ContactCallback : public btCollisionWorld::ContactResultCallback
{
public:
	void StartQuery()
	{
		m_foundCollision = false;
	}

	bool FoundCollision() const
	{
		return m_foundCollision;
	}

	virtual	btScalar addSingleResult(btManifoldPoint& cp, const btCollisionObjectWrapper* /*colObj0Wrap*/, int /*partId0*/, int /*index0*/, const btCollisionObjectWrapper* /*colObj1Wrap*/, int /*partId1*/, int /*index1*/)
	{
#if DEBUG_DRAWING
		btVector3 ptA = cp.getPositionWorldOnA();
		btVector3 ptB = cp.getPositionWorldOnB();
		g_debugDrawer.drawLine(ptA, ptB, btVector3(1.0f, 0.0f, 0.0f));
#else
		cp;
#endif

		m_foundCollision = true;
		return 0.0f;
	}

private:
	bool m_foundCollision;
};

extern "C" DLLEXPORT int CollisionCheck(const float* angles, float cageSpacer, int cageAttachment, float cameraDepth, float lensDepth, float riserHeight, float platformHeight, float platformRadius)
{
	unsigned int enableMask = 0xffffffff;

	if(platformHeight > 0.0f)
	{
		btCollisionObject* platform = g_objects[1];
		btBoxShape* platformShape = (btBoxShape*)platform->getCollisionShape();
		platformShape->setLocalScaling(btVector3(platformRadius * 2.0f, platformHeight, platformRadius * 2.0f));
		
		btTransform xform;
		xform.setIdentity();
		xform.setOrigin(btVector3(0.0f, 0.5f * platformHeight, 0.0f));
		platform->setWorldTransform(xform);
	}
	else
		enableMask &= ~0x02;

	if(riserHeight > 0.0f)
	{
		btCollisionObject* riser = g_objects[2];
		btBoxShape* riserShape = (btBoxShape*)riser->getCollisionShape();
		riserShape->setLocalScaling(btVector3(g_riserWidth, riserHeight, g_riserWidth));
		
		btTransform xform;
		xform.setIdentity();
		xform.setOrigin(btVector3(0.0f, platformHeight + 0.5f * riserHeight, 0.0f));
		riser->setWorldTransform(xform);
	}
	else
		enableMask &= ~0x04;

	float baseY = platformHeight + riserHeight;
	btTransform xform;
	xform.setIdentity();
	xform.setOrigin(btVector3(0.0f, baseY, 0.0f));
	g_objects[3]->setWorldTransform(xform);

	for(int i = 0; i < 5; ++i)
	{
		btTransform partXform(btQuaternion(g_boneAxes[i], angles[i]), btVector3(0.0f, g_boneHeights[i], 0.0f));
		xform = xform * partXform;
		g_objects[4 + i]->setWorldTransform(xform);
	}

	btTransform flangeXform(btQuaternion(g_boneAxes[5], angles[5]), btVector3(0.0f, g_boneHeights[5] + cageSpacer, 0.0f));
	xform = xform * flangeXform;

	btTransform cageLocalXform;
	switch(cageAttachment)
	{
		case 1:
		{
			btTransform pivot(btQuaternion::getIdentity(), btVector3(0.0f, -0.5f * g_cageSize.y(), 0.0f));
			btTransform rot(btQuaternion(g_xAxis, 0.5f * (float)M_PI), btVector3(0.0f, 0.0f, 0.0f));
			cageLocalXform = pivot.inverse() * rot * pivot;
			break;
		}

		case 2:
		{
			btTransform pivot(btQuaternion::getIdentity(), btVector3(0.0f, -0.5f * g_cageSize.y(), 0.0f));
			btTransform rot(btQuaternion(g_xAxis, -0.5f * (float)M_PI), btVector3(0.0f, 0.0f, 0.0f));
			cageLocalXform = pivot.inverse() * rot * pivot;
			break;
		}

		default:
			cageLocalXform.setIdentity();
			break;
	}

	btCollisionObject* cage = g_objects[9];
	btTransform cageWorldXform = xform * cageLocalXform;
	cage->setWorldTransform(cageWorldXform);

	btCollisionObject* cameraBox = g_objects[10];
	btBoxShape* cameraBoxShape = (btBoxShape*)cameraBox->getCollisionShape();
	float camBoxDepth = cameraDepth + lensDepth;
	cameraBoxShape->setLocalScaling(btVector3(camBoxDepth, g_camBoxHeight, g_camBoxHeight));

	btTransform camBoxLocalXform(btQuaternion::getIdentity(), btVector3(0.5f * camBoxDepth - lensDepth, 0.5f * g_camBoxHeight + g_cageWallHeight, 0.0f));
	btTransform camBoxWorldXform = xform * camBoxLocalXform;
	cameraBox->setWorldTransform(camBoxWorldXform);

#if DEBUG_DRAWING
	g_debugDrawer.Start();
	DebugDrawWorld(enableMask);
#endif

	ContactCallback contactCallback;
	contactCallback.StartQuery();

	for(int i = 0; (i < 9) && !contactCallback.FoundCollision(); ++i)
	{
		if( (enableMask & (1 << i)) == 0 )
			continue;

		// The last robot part is at index 9, but that's the wrist, and the cage is in contact with it by design, so we ignore it.
		if(i < 8)
			g_world->contactPairTest(cage, g_objects[i], contactCallback);

		if(!contactCallback.FoundCollision())
			g_world->contactPairTest(cameraBox, g_objects[i], contactCallback);
	}

#if DEBUG_DRAWING
	g_debugDrawer.Finish();
#endif

	return contactCallback.FoundCollision();
}
