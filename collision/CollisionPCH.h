#ifndef COLLISIONPCH_H
#define COLLISIONPCH_H

#if defined(_WIN32)
#	define DLLEXPORT					__declspec(dllexport)
#	define WIN32_LEAN_AND_MEAN
#	define NOMINMAX
#	define _WIN32_WINNT					0x0500
#	define WINVER						0x0500
#	define _CRT_SECURE_NO_DEPRECATE		1
#	define _CRT_NONSTDC_NO_DEPRECATE	1
#	define STRICT
#	define _USE_MATH_DEFINES
#elif defined(__APPLE__)
#	define DLLEXPORT					__attribute__((visibility("default")))
#else
#	error Unknown platform.
#endif

#include <vector>
#include <cstdio>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <assert.h>
#include <ctype.h>

#ifdef _WIN32
#	pragma warning(push)
#	pragma warning(disable: 4127)
#endif
#include <btBulletCollisionCommon.h>
#include <BulletCollision/CollisionShapes/btConvexPolyhedron.h>
#include <BulletCollision/CollisionShapes/btShapeHull.h>
#include <LinearMath/btIDebugDraw.h>
#ifdef _WIN32
#	pragma warning(pop)
#endif

#endif
