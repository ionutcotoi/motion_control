#include "CollisionPCH.h"
#include "DebugDrawer.h"

DebugDrawer::DebugDrawer()
{
	m_vtxBuf.reserve(1000);
	m_idxBuf.reserve(1000);
}

void DebugDrawer::drawLine(const btVector3& from, const btVector3& to, const btVector3& /*color*/)
{
	int idx = (int)m_vtxBuf.size();
	m_vtxBuf.push_back(from);
	m_vtxBuf.push_back(to);
	m_vtxBuf.push_back(from);
	m_idxBuf.push_back(idx);
	m_idxBuf.push_back(idx + 1);
	m_idxBuf.push_back(idx + 2);
}

void DebugDrawer::drawTriangle(const btVector3& a, const btVector3& b, const btVector3& c, const btVector3& /*color*/, btScalar /*alpha*/)
{
	int idx = (int)m_vtxBuf.size();
	m_vtxBuf.push_back(a);
	m_vtxBuf.push_back(b);
	m_vtxBuf.push_back(c);
	m_idxBuf.push_back(idx);
	m_idxBuf.push_back(idx + 1);
	m_idxBuf.push_back(idx + 2);
}

void DebugDrawer::reportErrorWarning(const char* warningString)
{
	printf("DebugDrawer: %s\n", warningString);
}

void DebugDrawer::Start()
{
	m_vtxBuf.resize(0);
	m_idxBuf.resize(0);
}

void DebugDrawer::Finish()
{
#ifdef _WIN32
	const char* fname = "c:\\temp\\collision_debug.obj";
#else
	const char* fname = "/tmp/collision_debug.obj";
#endif

	FILE* fp = fopen(fname, "w");
	if(!fp)
		return;

	for(size_t i = 0; i < m_vtxBuf.size(); ++i)
		fprintf(fp, "v %.6f %.6f %.6f\n", m_vtxBuf[i].x, m_vtxBuf[i].y, m_vtxBuf[i].z);

	fprintf(fp, "vt 0 0\nvn 0 1 0\n");

	for(size_t i = 0; i < m_idxBuf.size(); i += 3)
		fprintf(fp, "f %u/1/1 %u/1/1 %u/1/1\n", m_idxBuf[i] + 1, m_idxBuf[i + 1] + 1, m_idxBuf[i + 2] + 1);

	fclose(fp);
}

void DebugDrawer::DrawSolidBox(const btBoxShape* box, const btTransform& xform)
{
	int startIdx = (int)m_vtxBuf.size();

	for(int i = 0; i < 8; ++i)
	{
		btVector3 v;
		box->getVertex(i, v);
		m_vtxBuf.push_back(xform * v);
	}

	static const int idxBuf[] =
	{
		0, 1, 2, 3,
		4, 5, 6, 7,
		2, 0, 6, 4,
		7, 5, 3, 1,
		0, 1, 4, 5,
		7, 3, 6, 2
	};

	for(int i = 0; i < 6; ++i)
	{
		m_idxBuf.push_back(startIdx + idxBuf[4*i + 0]); m_idxBuf.push_back(startIdx + idxBuf[4*i + 1]); m_idxBuf.push_back(startIdx + idxBuf[4*i + 2]);
		m_idxBuf.push_back(startIdx + idxBuf[4*i + 2]); m_idxBuf.push_back(startIdx + idxBuf[4*i + 1]); m_idxBuf.push_back(startIdx + idxBuf[4*i + 3]);
	}
}

void DebugDrawer::DrawHull(const btConvexHullShape* hull, const btTransform& xform)
{
	const btConvexPolyhedron* poly = hull->getConvexPolyhedron();

	int startIdx = (int)m_vtxBuf.size();
	for(int i = 0; i <poly->m_vertices.size(); ++i)
		m_vtxBuf.push_back(xform * poly->m_vertices[i]);

	for(int i = 0; i < poly->m_faces.size(); ++i)
	{
		const btFace& face = poly->m_faces[i];
		int numVerts = face.m_indices.size();
		if(numVerts < 3)
			continue;

		int v0 = face.m_indices[0];
		for(int v = 0; v < numVerts - 2; ++v)
		{
			m_idxBuf.push_back(v0 + startIdx);
			m_idxBuf.push_back(face.m_indices[v + 1] + startIdx);
			m_idxBuf.push_back(face.m_indices[v + 2] + startIdx);
		}
	}
}
