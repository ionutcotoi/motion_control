#include "CollisionPCH.h"
#include "cd_wavefront.h"

static void GenerateHull(FILE* fp, const char* objName, int objIdx)
{
	char partFile[256];
	sprintf(partFile, "parts/%02d_%s.obj", objIdx, objName);
	ConvexDecomposition::WavefrontObj wo;
	wo.loadObj(partFile);

	btTriangleMesh* trimesh = new btTriangleMesh();

	for(int i = 0; i < wo.mTriCount; ++i)
	{
		int index0 = wo.mIndices[i*3];
		int index1 = wo.mIndices[i*3+1];
		int index2 = wo.mIndices[i*3+2];

		btVector3 vertex0(wo.mVertices[index0*3], wo.mVertices[index0*3+1], wo.mVertices[index0*3+2]);
		btVector3 vertex1(wo.mVertices[index1*3], wo.mVertices[index1*3+1], wo.mVertices[index1*3+2]);
		btVector3 vertex2(wo.mVertices[index2*3], wo.mVertices[index2*3+1], wo.mVertices[index2*3+2]);

		trimesh->addTriangle(vertex0,vertex1,vertex2);
	}

	btConvexShape* convexShape = new btConvexTriangleMeshShape(trimesh);
	convexShape->setMargin(0.0f);

	printf("old numTriangles = %d\n", wo.mTriCount);
	printf("old numIndices = %d\n", wo.mTriCount*3);
	printf("old numVertices = %d\n", wo.mVertexCount);

	printf("reducing vertices by creating a convex hull\n");

	// Create a hull approximation.
	btShapeHull* hull = new btShapeHull(convexShape);
	btScalar margin = convexShape->getMargin();
	hull->buildHull(margin);
	convexShape->setUserPointer(hull);

	printf("new numTriangles = %d\n", hull->numTriangles());
	printf("new numIndices = %d\n", hull->numIndices());
	printf("new numVertices = %d\n", hull->numVertices());

	fprintf(fp, "static const float g_%sVerts[] =\n{", objName);

	for(int i = 0; i < hull->numVertices(); ++i)
	{
		const btVector3& vert = hull->getVertexPointer()[i];
		if(i % 3 == 0)
			fprintf(fp, "\n\t");
		fprintf(fp, "%+.11ff, %+.11ff, %+.11ff, ", vert.x(), vert.y(), vert.z());
	}
	fprintf(fp, "\n};\n\n");

	delete convexShape;
	delete hull;
	delete trimesh;
}

void GenerateAllHulls()
{
	FILE* fp = fopen("hulls.h", "w");
	if(!fp)
		return;

	fprintf(fp, "#ifndef HULLS_H\n#define HULLS_H\n\n");

	static const int numObjs = 7;
	static const char* objNames[numObjs] =
	{
		"base",
		"shoulder",
		"arm",
		"elbow",
		"forearm",
		"wrist",
		"cage"
	};

	for(int i = 0; i < numObjs; ++i)
		GenerateHull(fp, objNames[i], i);

	fprintf(fp, "struct HullInfo\n{\n\tconst float* points;\n\tint numPoints;\n};\n\n");
	fprintf(fp, "static HullInfo g_hullInfo[] =\n{\n");
	for(int i = 0; i < numObjs; ++i)
		fprintf(fp, "\t{ g_%sVerts, sizeof(g_%sVerts) / sizeof(g_%sVerts[0]) / 3 },\n", objNames[i], objNames[i], objNames[i]);

	fprintf(fp, "};\n\nstatic const int g_numHulls = sizeof(g_hullInfo) / sizeof(g_hullInfo[0]);\n\n");
	fprintf(fp, "#endif\n");
	fclose(fp);
}
