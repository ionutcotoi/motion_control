#!/bin/bash

SOPATH=../addons/motion_control/collision_osx.so
g++ -O2 -DNDEBUG -arch x86_64 -fPIC -fvisibility=hidden -shared -o ${SOPATH} -Ibullet/include -Lbullet/lib_osx DebugDrawer.cpp cd_wavefront.cpp collision.cpp -lBulletCollision -lLinearMath
strip -x ${SOPATH}
rm -fr ${SOPATH}.dSYM
