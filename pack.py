#!/usr/bin/python2.7
# -*- coding: utf-8 -*-

import sys
import os 
import subprocess
import re

def PackWindows(archName):
    import _winreg
    try:
        hkey = _winreg.OpenKey(_winreg.HKEY_LOCAL_MACHINE, 'Software\\7-Zip')
        sevenzPath = _winreg.QueryValueEx(hkey, 'Path')[0]
    except EnvironmentError:
        print('Cannot find 7z.')
        sys.exit(1)

    os.chdir('addons')
    command = '"' + os.path.join(sevenzPath, '7z.exe') + '" a "..\\' + archName + '" * -xr!"*.blend?" -xr!"*.pyc" -xr!"__pycache__"'
    subprocess.call(command, stdout=sys.stdout, stderr=sys.stdout, shell=True)

def PackUnix(archName):
    os.chdir('addons')
    command = 'zip -9r "../' + archName + '" * -x "*.blend?" "*.pyc" "__pycache__"'
    subprocess.call(command, stdout=sys.stdout, stderr=sys.stdout, shell=True)

verStr = None

f = open(os.path.join('addons', 'motion_control', '__init__.py'), 'r')
for l in f:
    m = re.match(r'.*"version".+\((.+)\).*', l)
    if m is not None:
        ver = m.groups()[0].split(',')
        if len(ver) == 3:
            verStr = '.'.join([field.strip() for field in ver])
            break
f.close()

if verStr is None:
    print('Cannot read version.')
    sys.exit(1)

rev = subprocess.check_output('git rev-parse --short HEAD', shell=True).strip()
verStr = verStr + '.' + rev

archName = 'motion_control-' + verStr + '.zip'
if os.path.exists(archName):
    os.remove(archName)

if os.name == "nt":
    PackWindows(archName)
else:
    PackUnix(archName)
