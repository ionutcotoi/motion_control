#!/usr/bin/python
# -*- coding: utf-8 -*-

import math
import maya.cmds as cmds
from maya.OpenMaya import *

def ShowError(msg):
    cmds.confirmDialog(title='Error', message=msg, button=['Close'], defaultButton='Close', cancelButton='Close', dismissString='Close')

def GetCameraControls():
    selection = cmds.ls(selection = True)
    camShape = None
    camXform = None
    for item in selection:
        if cmds.nodeType(item) != 'transform':
            continue
        shapes = cmds.listRelatives(item, shapes = True)
        for shape in shapes:
            if cmds.nodeType(shape) == 'camera':
                camShape = shape
                camXform = item
                break
        if camShape:
            break

    if not camShape:
        ShowError('Please select a camera.')
        return (None, None, None)

    inputs = cmds.listConnections(camXform + '.rotateX', source = True)
    if not inputs:
        ShowError('Please set the camera controls to "Camera, Aim, and Up".')
        return (None, None, None)
    camGrp = inputs[0]

    inputs = cmds.listConnections(camGrp + '.worldUpMatrix', source = True)
    if not inputs:
        ShowError('Please set the camera controls to "Camera, Aim, and Up".')
        return (None, None, None)
    upNode = inputs[0]

    inputs = cmds.listConnections(camGrp + '.target[0].targetParentMatrix', source = True)
    if not inputs:
        ShowError('Please set the camera controls to "Camera, Aim, and Up".')
        return (None, None, None)
    aimLocator = inputs[0]

    print('Using camera ' + camXform + ', aim locator ' + aimLocator + ', up node ' + upNode + '.')
    return (camXform, aimLocator, upNode)
    
    
def CreateImportDialog(importPath):
    exportWnd = cmds.window("MImpImportWindow")
    exportForm = cmds.formLayout()
    cmds.columnLayout()
    cmds.rowLayout()
    exportName = cmds.textFieldGrp("MImpImportLocation", label = "Import Path", text=importPath, editable=False)
    cmds.setParent("..")
    cmds.rowLayout()
    scaleField = cmds.floatFieldGrp("MImpImportScale", label="Global Scale", value1=1.0)
    cmds.setParent("..")
    cmds.rowLayout(columnAlign1="center", columnAttach1="left", columnOffset1=150)
    exportButton = cmds.button("MImpImportButton", label="Import", command=DoMotionImport, width=100)
    cmds.showWindow()

def ImportCameraMotion():
    camXform, aimLocator, upNode = GetCameraControls()
    if(camXform == None or aimLocator == None or upNode == None):
        return

    extFilter = 'Keys files (*.keys);;All Files (*.*)'
    filename = cmds.fileDialog2(fileMode=1, caption='Import Motion', fileFilter=extFilter)
    if filename == None:
        return

    filename = filename[0]
    CreateImportDialog(filename)

def SetVectorKeys(frame, node, blenderVect, scale):
    # Convert from Blender space to Maya space.
    mayaVect = [ float(cmds.convertUnit(blenderVect[i], fromUnit='m')) * scale for i in [0, 2, 1] ]
    mayaVect[2] = -mayaVect[2]

    attrs = ['translateX', 'translateY', 'translateZ']
    for i in range(3):
        cmds.setKeyframe(node, attribute=attrs[i], t=frame, v=mayaVect[i], hierarchy='none', shape=False, inTangentType='linear', outTangentType='linear')

def DoMotionImport(*args):
    camXform, aimLocator, upNode = GetCameraControls()
    if (camXform == None) or (aimLocator == None) or (upNode == None):
        return

    filename = cmds.textFieldGrp("MImpImportLocation", text=True, query=True)
    globalScale = cmds.floatFieldGrp("MImpImportScale", query=True, value1=True)
    cmds.deleteUI("MImpImportWindow")
    
    try:
        f = open(filename, 'r')
    except IOError as e:
        ShowError('Cannot open file "' + filename + '": ' + str(e) + '.')
        return

    # Clear the translation keys on the camera controls.
    for node in [camXform, aimLocator, upNode]:
        for comp in ['X', 'Y', 'Z']:
            cmds.cutKey(node, attribute=('translate' + comp), clear=True, hierarchy='none', controlPoints=False, shape=True)

    maxTime = 0
    cmds.keyTangent(g=True, wt=False)
    for l in f:
        lineFields = l.strip().split(' ')
        if len(lineFields) != 10:
            continue

        frame = lineFields[0]
        SetVectorKeys(frame, camXform, lineFields[1:4], globalScale)
        SetVectorKeys(frame, aimLocator, lineFields[4:7], globalScale)
        SetVectorKeys(frame, upNode, lineFields[7:10], globalScale)

        if float(frame) > maxTime:
            maxTime = float(frame)
        
    f.close()

    crMaxTime = cmds.playbackOptions(query=True, maxTime=True)
    if crMaxTime < maxTime:
        cmds.playbackOptions(maxTime=maxTime)
        
def convertMayaPos(node):
    mayaPos = cmds.getAttr(node + '.translate')[0]

    blenderPos = [ float(cmds.convertUnit(mayaPos[i], toUnit='m')[:-1]) for i in [0, 2, 1] ]
    blenderPos[1] = -blenderPos[1]
    return blenderPos
    
def DoMotionExport(filename):
    camXform, aimLocator, upNode = GetCameraControls()
    if (camXform == None) or (aimLocator == None) or (upNode == None):
        return
    
    try:
        keyFile = open(filename, 'w')
    except IOError as e:
        ShowError('Cannot open file "' + filename + '": ' + str(e) + '.')

    firstFrame = 10000
    lastFrame = -10000
    for attr in [ (camXform, 'translate'), (aimLocator, 'translate'), (upNode, 'translate') ]:
        numKeys = cmds.keyframe(attr[0], q=True, shape=False, attribute=attr[1], hierarchy='none', kc=True)
        times = cmds.keyframe(attr[0], query=True, index=(0, numKeys - 1), attribute=attr[1], timeChange=True, absolute=True)
        minTime = int(min(times))
        maxTime = int(max(times))
        firstFrame = min(minTime, firstFrame)
        lastFrame = max(maxTime, lastFrame)

    savedFrame = cmds.currentTime(query=True)

    for frame in range(firstFrame, lastFrame + 1):
        cmds.currentTime(frame)
        camPos = convertMayaPos(camXform)
        targetPos = convertMayaPos(aimLocator)
        upPos = convertMayaPos(upNode)

        keyFile.write('%d %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f\n' % (
            frame, 
            camPos[0], camPos[1], camPos[2],
            targetPos[0], targetPos[1], targetPos[2],
            upPos[0], upPos[1], upPos[2]))

    keyFile.close()
    cmds.currentTime(savedFrame)

def ExportCameraMotion():
    camXform, aimLocator, upNode = GetCameraControls()
    if camXform == None or aimLocator == None or upNode == None:
        return

    extFilter = 'Keys files (*.keys);;All Files (*.*)'
    filename = cmds.fileDialog2(fileMode=0, caption='Export Motion', fileFilter=extFilter)
    if filename == None:
        return
    
    filename = filename[0]
    if filename.endswith('.*'):
        filename = filename[:-1] + 'keys'

    DoMotionExport(filename)
