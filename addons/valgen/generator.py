#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import sys
import shutil
from ftplib import FTP
from jinja2 import Environment
from jinja2.loaders import FileSystemLoader
import motion_control.solver
import math
import serial
from time import sleep

VAL3_TEMPLATES_PATH    = os.path.join(os.path.abspath(os.path.dirname(__file__)), r"templates")
VAL3_STATIC_PROGS_PATH = os.path.join(VAL3_TEMPLATES_PATH, r"static")

class Val3Generator:
    """VAL3 project generator class"""

    templates = {
        "datadef": {"name":"debugi.dtx", "extension": ".dtx"},
        "externs": {"name":"debugi.ltx", "extension": ".ltx"},
        "project": {"name":"debugi.pjx", "extension": ".pjx"},
        "start":   {"name":"start.pgx",  "extension": ".pgx"},
        "stop":    {"name":"stop.pgx",   "extension": ".pgx"},
        "acheck":  {"name":"acheck.pgx", "extension": ".pgx"},
    }

    project = {
        "datadef": {"path": "", "filename": ""},
        "externs": {"path": "", "filename": ""},
        "project": {"path": "", "filename": ""},
        "start":   {"path": "", "filename": ""},
        "stop":    {"path": "", "filename": ""},
        "acheck":   {"path": "", "filename": ""},
    }

    def __init__(self, steps, points, output_project_name, fps, keyframes, ext_triggers):
        self.output_project = output_project_name
        self.steps = []
        self.base_dir = ""
        self.steps = steps
        self.speeds = []
        self.fps = fps

        self.nom_speeds = [math.degrees(x) for x in motion_control.solver.g_jointNomSpeeds]

        for i in range(1, len(self.steps)):
            v = [abs(self.steps[i][0] - self.steps[i-1][0]) * self.fps/self.nom_speeds[0]*100,
                 abs(self.steps[i][1] - self.steps[i-1][1]) * self.fps/self.nom_speeds[1]*100,
                 abs(self.steps[i][2] - self.steps[i-1][2]) * self.fps/self.nom_speeds[2]*100,
                 abs(self.steps[i][3] - self.steps[i-1][3]) * self.fps/self.nom_speeds[3]*100,
                 abs(self.steps[i][4] - self.steps[i-1][4]) * self.fps/self.nom_speeds[4]*100,
                 abs(self.steps[i][5] - self.steps[i-1][5]) * self.fps/self.nom_speeds[5]*100]
            ang_speed = float(max(v))
            ang_speed = max(ang_speed, 1)

            self.speeds.append(ang_speed)

        self.speeds.insert(0, self.speeds[0])

        self.points = points
        self.keyframes = keyframes

        self.env = Environment(loader=FileSystemLoader(VAL3_TEMPLATES_PATH))

    def gen_dtx_file(self):
        dtx_tpl = self.env.get_template(self.templates['datadef']['name'])
        dtx = dtx_tpl.render(steps = self.steps, speeds = self.speeds, points = self.points, keyframes = self.keyframes)

        filename = self.output_project+self.templates['datadef']['extension']
        filepath = os.path.join(self.base_dir, filename)

        self.project["datadef"]["path"] = filepath
        self.project["datadef"]["filename"] = filename

        with open(filepath, "w") as outfile:
            outfile.write(dtx)

    def gen_ltx_file(self):
        ltx_tpl = self.env.get_template(self.templates['externs']['name'])
        ltx = ltx_tpl.render()

        filename = self.output_project+self.templates['externs']['extension']
        filepath = os.path.join(self.base_dir, filename)

        self.project["externs"]["path"] = filepath
        self.project["externs"]["filename"] = filename

        #print filename
        with open(filepath, "w") as outfile:
            outfile.write(ltx)

    def gen_project_file(self):
        pjx_tpl = self.env.get_template(self.templates['project']['name'])
        pjx = pjx_tpl.render(project_name = self.output_project)

        filename = self.output_project+self.templates['project']['extension']
        filepath = os.path.join(self.base_dir, filename)

        self.project["project"]["path"] = filepath
        self.project["project"]["filename"] = filename

        #print filename
        with open(filepath, "w") as outfile:
            outfile.write(pjx)

    def gen_start_stop_files(self):
        start_tpl = self.env.get_template(self.templates['start']['name'])
        pgx = start_tpl.render(numSteps = len(self.steps) - 1, name = self.output_project)

        filename = os.path.join(self.base_dir, self.templates['start']['name'])

        self.project["start"]["path"] = filename
        self.project["start"]["filename"] = self.templates['start']['name']

        #print filename
        with open(filename, "w") as outfile:
            outfile.write(pgx)

        stop_tpl = self.env.get_template(self.templates['stop']['name'])
        pgx = stop_tpl.render()

        filename = os.path.join(self.base_dir, self.templates['stop']['name'])

        self.project["stop"]["path"] = filename
        self.project["stop"]["filename"] = self.templates['stop']['name']

        self.project["acheck"]["path"] = filename
        self.project["acheck"]["filename"] = self.templates['acheck']['name']

        #print filename
        with open(filename, "w") as outfile:
            outfile.write(pgx)

    def writeProject(self, path):
        target_dir = os.path.join(path, self.output_project)
        try:
            os.mkdir(target_dir)
        except OSError:
            shutil.rmtree(target_dir)
            os.mkdir(target_dir)

        self.base_dir = target_dir
        self.gen_dtx_file()
        self.gen_ltx_file()
        self.gen_project_file()
        self.gen_start_stop_files()
        pass

    def uploadToRobot(self, hostname, user, passwd):
        try:
            ftp = FTP(hostname, user, passwd, timeout=10)
        except:
            #FIXME show connect error to the user
            raise
        else:
            ftp.cwd("/usr/usrapp")
            try:
                ftp.mkd(self.output_project)
            except Exception as e:
                print("Exc 1:", e)
            ftp.cwd(self.output_project)
            for pfile in self.project.items():
                #print pfile[1]
                with open(pfile[1]['path'], "rb") as infile:
                    ftp.storbinary("STOR " + pfile[1]['filename'], infile)

            for proj in os.walk(VAL3_STATIC_PROGS_PATH):
                if os.name == "nt":
                    proj_name = proj[0].split("\\")[-1]
                else:
                    proj_name = proj[0].split("/")[-1]
                if proj_name != 'static':
                    ftp.cwd("/usr/usrapp")
                    print ("Static proj:", proj_name)
                    try:
                        ftp.mkd(proj_name)
                    except Exception as e:
                        print("Exc 2:", e)
                    try:
                        ftp.cwd(proj_name)
                        for pfile in proj[2]:
                            filename = os.path.join(proj[0], pfile)
                            with open(filename, "rb") as infile:
                                ftp.storbinary("STOR " + pfile, infile)
                    except Exception as e:
                        print("Exc 3:", e)

            ftp.quit()

    def programInterfaceBox(self, serial_port, extChanValues, extChanStates, extChanNames, extChanTriggers, fps):
        intfSerialPort = None
        fps = int(fps)
        # print("extChanNames", extChanNames)
        # print("extChanValues", extChanValues)
        # print("extChanStates", extChanStates)
        # print("fps", fps)
        device_names = ['F0','F1','S0','S1','D0','D1','D2','D3']

        print ("extChanTriggers:", extChanTriggers)

        intfSerialPort = serial.Serial(port=serial_port, baudrate=115200, timeout=10)

        if intfSerialPort and intfSerialPort.isOpen():
            print("Programming the box")

            msg = intfSerialPort.read(2)
            print("Box Msg:", msg.decode("ascii"))

            # Read the OK from the box
            msg = ""
            while intfSerialPort.inWaiting() > 0:
                msg += intfSerialPort.read(1).decode("ascii")

            print(msg)


            # Store the focus values into the controller
            print(extChanValues)
            for frame, values in enumerate(extChanValues):
                for device_index, device in enumerate(extChanNames):
                    if extChanStates[device_index]:
                        if device not in device_names:
                            print('Error: "'+device+'" not allowed as an output name. Output names should be one of: '+ ' '.join(map(str,device_names)))
                            return;
                        else:
                            cmdSTOR = "STOR " + device + ' ' + str(frame) + ' ' + str(int(values[device_index])) + "\n"
                            cmdSTOR = bytes(cmdSTOR.encode('ascii'))

                            nrbytes = intfSerialPort.write(cmdSTOR)
                            intfSerialPort.flush()

                            if nrbytes > 0:
                                print("Wrote", nrbytes, "bytes :", cmdSTOR)
                            else:
                                print("ERROR writing STOR:", cmdSTOR)
                            msg = ""
                            while intfSerialPort.inWaiting() > 0:
                                msg += intfSerialPort.read(1).decode("ascii")
                            print(msg)
                            sleep(0.05)

            # Store the fps and number of frames into the controller
            cmdBFPS = "BFPS " + str(fps).ljust(4) + " " + str(len(extChanValues)) + "\n"
            intfSerialPort.write(cmdBFPS.encode('ascii'))
            print(cmdBFPS)
            intfSerialPort.close()
        else:
            print("Cannot connect to the box serial port")

# val3gen = Val3Generator("/Users/cotty/dev/motion_control/addons/motion_control/untitled.cam", "test")
# val3gen.writeProject("/Users/cotty/Desktop")
# val3gen.uploadToRobot("localhost", "maintenance", "spec_cal")
