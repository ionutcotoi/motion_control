#!/usr/bin/python
# -*- coding: utf-8 -*-

bl_info = {
    "name":         "Motion Control",
    "description":  "Camera motion control with a Staubli RX130B robot arm",
    "author":       "Griffin Software",
    "version":      (1, 1, 19),
    "blender":      (2, 68, 0),
    "location":     "Properties > Scene",
    "category":     "Animation"
}

import bpy
from bpy.app.handlers import persistent
from motion_control.ui import *
import motion_control.solver
import motion_control.joystick
import motion_control.ui

def RegisterDriverFuncs():
    bpy.app.driver_namespace['solveShoulder'] = motion_control.solver.solveShoulder
    bpy.app.driver_namespace['solveArm'] = motion_control.solver.solveArm
    bpy.app.driver_namespace['solveElbow'] = motion_control.solver.solveElbow
    bpy.app.driver_namespace['solveForearm'] = motion_control.solver.solveForearm
    bpy.app.driver_namespace['solveWrist'] = motion_control.solver.solveWrist
    bpy.app.driver_namespace['solveFlange'] = motion_control.solver.solveFlange
    bpy.app.driver_namespace['solveReachability'] = motion_control.solver.solveReachability
    bpy.app.driver_namespace['getCagePosition'] = motion_control.solver.getCagePosition
    bpy.app.driver_namespace['getCageRotation'] = motion_control.solver.getCageRotation
    bpy.app.driver_namespace['getDrivenCamPosition'] = motion_control.solver.getDrivenCamPosition
    bpy.app.driver_namespace['getDrivenCamRotation'] = motion_control.solver.getDrivenCamRotation
    bpy.app.driver_namespace['getCameraBboxTransform'] = motion_control.solver.getCameraBboxTransform

def RegisterProperties():
    bpy.types.Object.rig_properties = bpy.props.PointerProperty(type=RigProperties)

def UpdateRig():
    if bpy.data.objects.find('Camera_Position') == -1:
        # The rig isn't present.
        return

    if bpy.data.objects.find('Camera_Pole') == -1:
        # The rig doesn't have the pole vector control. Add it.
        bpy.ops.object.empty_add(type='SPHERE', location=(0, 0, 0.5))
        poleLocator = bpy.context.object
        poleLocator.name = 'Camera_Pole'
        poleLocator.parent = bpy.data.objects['Camera_Position']
        poleLocator.empty_draw_size = 0.1
        poleLocator.show_name = True
        poleLocator.show_x_ray = True

@persistent
def PreLoadHook(scene):
    motion_control.ui.StartJoystickOp.stop(bpy.context, True)

@persistent
def PostLoadHook(scene):
    RegisterDriverFuncs()
    RegisterProperties()
    UpdateRig()

def register():
    if not PreLoadHook in bpy.app.handlers.load_pre:
        bpy.app.handlers.load_pre.append(PreLoadHook)

    if not PostLoadHook in bpy.app.handlers.load_post:
        bpy.app.handlers.load_post.append(PostLoadHook)

    bpy.utils.register_module(__name__)
    motion_control.joystick.init()

def unregister():
    if PreLoadHook in bpy.app.handlers.load_pre:
        bpy.app.handlers.load_pre.remove(PreLoadHook)

    if PostLoadHook in bpy.app.handlers.load_post:
        bpy.app.handlers.load_post.remove(PostLoadHook)

    bpy.utils.unregister_module(__name__)
    motion_control.joystick.shutdown()
