#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import math
import bpy
from bpy.props import *
from bpy_extras.io_utils import ExportHelper
from bpy_extras.io_utils import ImportHelper
import bl_operators
from valgen.generator import Val3Generator
import motion_control.joystick
import motion_control.solver
import blf
import bgl
import mathutils
import traceback
import serial
import sys
import glob


def get_serial_ports():
    """Lists serial ports

    :raises EnvironmentError:
        On unsupported or unknown platforms
    :returns:
        A list of available serial ports
    """
    if sys.platform.startswith('win'):
        ports = ['COM' + str(i + 1) for i in range(256)]

    elif sys.platform.startswith('linux'):
        # this is to exclude your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')

    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')

    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for i, port in enumerate(ports):
        try:
            s = serial.Serial(port)
            s.close()
            t = (port, port, "", i)
            result.append(t)
        except (OSError, serial.SerialException):
            pass
    return result


def GatherKeyframes():
    keyframes = set()
    for objName in 'Camera_Target', 'Camera_Position', 'Camera_Pole', 'Robot':
        animData = bpy.data.objects[objName].animation_data
        if not animData:
            continue

        action = animData.action
        if not action:
            continue
            
        for fcurve in action.fcurves:
            for pnt in fcurve.keyframe_points:
                keyframes.add(pnt.co[0])

    return keyframes


def GetRobotExportData():
    robot = bpy.data.objects['Robot']

    currentFrame = bpy.context.scene.frame_current
    startFrame = bpy.context.scene.frame_start
    endFrame = bpy.context.scene.frame_end

    angles = []
    points = []
    extChanValues = []
    keyAngles = []
    invalidFrame = None
    keyframes = GatherKeyframes()
    frameDuration = bpy.context.scene.render.fps_base / bpy.context.scene.render.fps

    for frame in range(startFrame, endFrame + 1):
        bpy.context.scene.frame_set(frame)
        camPos, targetPos, _, _ = motion_control.solver.getRigControls()
        solution = motion_control.solver.solveRobot(camPos.x, camPos.y, camPos.z, targetPos.x, targetPos.y, targetPos.z)
        reachable = solution[6]
        if not reachable:
            print('Unreachable configuration in frame', frame)
            invalidFrame = frame
            break
        
        frameAngles = [math.degrees(x) for x in solution[:6]]
        #print(frame, '. Solution:    ', frameAngles)
        
        if frame > startFrame:
            prevAngles = angles[-1]
            frameAngles = motion_control.solver.computeShortestPath(prevAngles, frameAngles, frameDuration)
            if frameAngles is None:
                invalidFrame = frame
                break
            #print(frame, '. After deltas:', frameAngles, '\n')

        if invalidFrame is not None:
            break
        
        if frame in keyframes:
            keyAngles.append(frameAngles)

        angles.append(frameAngles)

        #if frame in keyframes:
        pos, rot, cfg = motion_control.solver.getEffectorPoint(frameAngles)
        points.append((pos, rot, cfg))


        frameExtVals = [ getattr(robot.rig_properties, 'extern_chan_%d_val' % i) for i in range(RigProperties.NUM_EXTERNAL_CHANS) ]
        extChanValues.append(frameExtVals)

    bpy.context.scene.frame_set(currentFrame)

    if invalidFrame is not None:
        bpy.ops.error.message('INVOKE_DEFAULT', message='ERROR: The motion reaches an unreachable configuration in frame ' + str(invalidFrame) + '.')
        return None
    
    return (angles, points, extChanValues, keyAngles)


def Export(filename, write_ext_chan):
    expData = GetRobotExportData()
    if expData is None:
        return

    robot = bpy.data.objects['Robot']
    angles, points, extChanValues, keyAngles = expData
    extChanStates = [ getattr(robot.rig_properties, 'extern_chan_%d_ena' % i) for i in range(RigProperties.NUM_EXTERNAL_CHANS) ]
    extChanNames = [ getattr(robot.rig_properties, 'extern_chan_%d_name' % i) for i in range(RigProperties.NUM_EXTERNAL_CHANS) ]
    extChanTriggers = robot.rig_properties.triggers

    projdir = os.path.dirname(filename)
    subdir = os.path.splitext(os.path.basename(filename))[0]
    fps = bpy.context.scene.render.fps / bpy.context.scene.render.fps_base

    valgen = Val3Generator(angles, points, subdir, fps, keyAngles, extChanTriggers)
    valgen.writeProject(projdir)
    try:
        valgen.uploadToRobot("192.168.10.158", "maintenance", "spec_cal")
        #valgen.uploadToRobot("localhost", "cotty", "peste")
    except:
        bpy.ops.error.message('INVOKE_DEFAULT', message="Unable to connect to the Robot!")

    # You can use writeExtChanProg here to know if you have to write the external channels program.
    if write_ext_chan['do_prog']:
        try:
            valgen.programInterfaceBox(write_ext_chan['serial_port'],
                                       extChanValues,
                                       extChanStates,
                                       extChanNames,
                                       extChanTriggers,
                                       fps)
        except Exception as e:
            print("Exception:", e)
            traceback.print_exc()
            bpy.ops.error.message('INVOKE_DEFAULT', message="Cannot program interface box!")


def ExportCameraKeys(filename):
    expData = GetRobotExportData()
    if expData is None:
        return

    try:
        keyFile = open(filename, 'w')
    except IOError as e:
        bpy.ops.error.message('INVOKE_DEFAULT', message="ERROR: cannot write to file '" + filename + '": ' + str(e) + '.')
        return
    
    currentFrame = bpy.context.scene.frame_current
    startFrame = bpy.context.scene.frame_start
    endFrame = bpy.context.scene.frame_end
        
    for frame in range(startFrame, endFrame + 1):
        bpy.context.scene.frame_set(frame)
        
        camPos, targetPos, poleVect, camRoll = motion_control.solver.getRigControls()
        _, _, upDir = motion_control.solver.computeCamAxes(camPos, targetPos, poleVect, camRoll)
        upPos = camPos - 0.2*upDir

        keyFile.write('%d %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f %.8f\n' % (
            frame, 
            camPos.x, camPos.y, camPos.z,
            targetPos.x, targetPos.y, targetPos.z,
            upPos.x, upPos.y, upPos.z))

    bpy.context.scene.frame_set(currentFrame)
    keyFile.close()


def resetObjAnim(obj, createEmpty):
    if obj.animation_data:
        obj.animation_data.action = None

    if not createEmpty:
        return None

    if not obj.animation_data:
        obj.animation_data_create()

    obj.animation_data.action = bpy.data.actions.new(name=obj.name+"_anim")
    animAction = obj.animation_data.action
    curves = [ animAction.fcurves.new(data_path="location", index=i) for i in range(3) ]
    return curves


def setVectorKeys(frame, curves, v):
    for i in range(3):
        curve = curves[i]
        curve.keyframe_points.add(1)
        newKeyIdx = len(curve.keyframe_points) - 1
        newKey = curve.keyframe_points[newKeyIdx]
        newKey.co = frame, v[i]


def ImportCameraAnim(filename, distScale):
    try:
        keyFile = open(filename, 'r')
    except IOError as e:
        bpy.ops.error.message('INVOKE_DEFAULT', message='ERROR: Cannot open the file ' + filename + '. Please try again.')
        return

    # Clear all keyframes from the robot (i.e. any existing roll keys). Afterwards, set the roll
    # to 0, because animation files contain explicit up vectors, which we'll use as pole vectors.
    robot = bpy.data.objects['Robot']
    resetObjAnim(robot, False)
    robot.rig_properties.cam_roll = 0

    posLocator = bpy.data.objects['Camera_Position']
    targetLocator = bpy.data.objects['Camera_Target']
    poleLocator = bpy.data.objects['Camera_Pole']

    animCurves = []
    for obj in [posLocator, targetLocator, poleLocator]:
        animCurves.append(resetObjAnim(obj, True))

    # Make sure the target and position locators aren't parented to anything, 
    # as that can screw things up.
    posLocator.parent = None
    targetLocator.parent = None

    for l in keyFile:
        lineFields = l.strip().split(' ')
        if len(lineFields) != 10:
            continue
        
        frame = int(lineFields[0])

        vects = [ mathutils.Vector([float(x) * distScale for x in lineFields[i:i+3]]) for i in [1, 4, 7] ]

        # The up locator is parented to the position locator, so we must compute its
        # relative position.
        vects[2] = vects[2] - vects[0]

        for i in range(3):
            setVectorKeys(frame, animCurves[i], vects[i])
            
    keyFile.close()


class ImportCameraAnimOp(bpy.types.Operator, ImportHelper):
    """Import camera animation into current scene"""
    bl_idname = 'motion_control.import_cam_anim'
    bl_label = 'Import Key File'
    filename_ext = '.keys';
    
    scale_prop = bpy.props.FloatProperty(name = 'Scale Factor', description = 'Scale factor to apply to the incoming positions', default = 1.0)

    @classmethod
    def poll(cls, context):
        return (bpy.data.objects.find('Robot') != -1)

    def execute(self, context):
        ImportCameraAnim(self.filepath, self.scale_prop)
        return {'FINISHED'}


class ImportRigOp(bpy.types.Operator):
    """Import motion control rig into current scene"""
    bl_idname = 'motion_control.import_rig'
    bl_label = 'Import Rig'

    @classmethod
    def poll(cls, context):
        return (bpy.data.objects.find('Robot') == -1)

    def execute(self, context):
        # Blender is so retarded that you can't tell it to import all the objects in a file. The API also doesn't have a way to list the contents of a given file.
        # This means we have to know the names of all the objects we want to import, and when we add more stuff to the rig file, we have to modify this list. RETARDED.
        files = [
            {'name':'Robot_Root'}, {'name':'Robot'}, {'name':'Base'}, {'name':'Shoulder'}, {'name':'Arm'}, {'name':'Elbow'}, {'name':'Forearm'}, {'name':'Wrist'}, {'name':'Flange'},
            {'name':'CameraCage'}, {'name':'DrivenCamera'}, {'name':'Riser'}, {'name':'Platform'}, {'name':'Camera_Bbox'},
            {'name':'Camera_Position'}, {'name':'Camera_Target'}, {'name':'Camera_Pole'}
        ]
        modDir = os.path.abspath(os.path.dirname(__file__))
        objDir = os.path.join(modDir, 'data', 'robot_rig.blend', 'Object', '')
        bpy.ops.wm.link_append(directory=objDir, files=files, link=False)

        # Move the objects to layers, since that information isn't imported for some stupid reason.
        for objName in ['Base', 'Shoulder', 'Arm', 'Elbow', 'Forearm', 'Wrist', 'Flange']:
            bpy.data.objects[objName].layers[1] = True
            bpy.data.objects[objName].layers[0] = False
            bpy.data.objects[objName].layers[2] = False

        bpy.context.scene.layers[0] = True
        bpy.context.scene.layers[1] = True
        bpy.context.scene.layers[2] = False

        return {'FINISHED'}


class SelectPosOp(bpy.types.Operator):
    """Select the camera position controller"""
    bl_idname = 'motion_control.select_pos'
    bl_label = 'Select Position'

    @classmethod
    def poll(cls, context):
        return (bpy.data.objects.find('Robot') != -1)

    def execute(self, context):
        bpy.ops.object.select_pattern(pattern='Camera_Position', case_sensitive=True, extend=False)
        return {'FINISHED'}


class SelectTargetOp(bpy.types.Operator):
    """Select the camera target controller"""
    bl_idname = 'motion_control.select_target'
    bl_label = 'Select Target'

    @classmethod
    def poll(cls, context):
        return (bpy.data.objects.find('Robot') != -1)

    def execute(self, context):
        bpy.ops.object.select_pattern(pattern='Camera_Target', case_sensitive=True, extend=False)
        return {'FINISHED'}


def ForceRedraw():
    bpy.ops.wm.redraw_timer(type='DRAW_WIN_SWAP', iterations=1)


class StartJoystickOp(bpy.types.Operator):
    """Start interactive rig control"""
    bl_idname = 'motion_control.start_joystick'
    bl_label = 'Start'

    joystick = None
    drawHandler = None
    timer = None

    @classmethod
    def poll(cls, context):
        return (bpy.data.objects.find('Robot') != -1) and (StartJoystickOp.joystick is None)

    @staticmethod
    def stop(context, loadingNewScene):
        if StartJoystickOp.timer:
            context.window_manager.event_timer_remove(StartJoystickOp.timer)
            StartJoystickOp.timer = None

        if StartJoystickOp.drawHandler:
            bpy.types.SpaceView3D.draw_handler_remove(StartJoystickOp.drawHandler, 'WINDOW')
            StartJoystickOp.drawHandler = None

        if StartJoystickOp.joystick:
            StartJoystickOp.joystick.stop()
            StartJoystickOp.joystick = None

            if not loadingNewScene:
                ForceRedraw()

    def DrawJoystickState(self, context):
        fontSize = 16
        topMargin = 10
        rightMargin = 20
        lineSpacing = 2

        robot = bpy.data.objects['Robot']

        text = []
        statusText = 'Joystick: '
        if self.joystick.disabled:
            statusText = statusText + 'DISABLED'
        else:
            statusText = statusText + 'ON'
        text.append(statusText)
        text.append('Selection: ' + self.joystick.selectedLocator)
        text.append('Space: ' + robot.rig_properties.joy_mode)
        text.append('Speed: %.1f%%' % self.joystick.speedMult)
        if self.joystick.serverThread.clientSocket is not None:
            text.append('Robot Connected')
        if getattr(robot.rig_properties, 'extern_chan_%d_ena' % self.joystick.selectedExtChan):
            chanName = getattr(robot.rig_properties, 'extern_chan_%d_name' % self.joystick.selectedExtChan)
            chanVal = getattr(robot.rig_properties, 'extern_chan_%d_val' % self.joystick.selectedExtChan)
            text.append('External channel: %d (%s): %.2f' % (self.joystick.selectedExtChan, chanName, chanVal))

        blf.size(0, fontSize, 72)
        blf.enable(0, blf.SHADOW)
        blf.shadow_offset(0, 1, -1)
        blf.shadow(0, 5, 0.0, 0.0, 0.0, 0.8)
        bgl.glColor4f(1, 1, 1, 1)

        y = context.region.height - topMargin
        for line in text:
            width, height = blf.dimensions(0, line)
            x = context.region.width - width - rightMargin
            y -= height
            blf.position(0, x, y , 0)
            blf.draw(0, line)
            y -= lineSpacing

        blf.blur(0,0)
        blf.disable(0, blf.SHADOW)

    def modal(self, context, event):
        if StartJoystickOp.joystick is None:
            return self.cancel(context)

        if event.type != 'TIMER':
            return {'PASS_THROUGH'}

        StartJoystickOp.joystick.run()
        return {'RUNNING_MODAL'}

    def execute(self, context):
        try:
            StartJoystickOp.joystick = motion_control.joystick.JoystickControl()
        except IOError as e:
            bpy.ops.error.message('INVOKE_DEFAULT', message='Error: ' + str(e))
            return {'FINISHED'}

        StartJoystickOp.drawHandler = bpy.types.SpaceView3D.draw_handler_add(self.DrawJoystickState, (context, ), 'WINDOW', 'POST_PIXEL')

        context.window_manager.modal_handler_add(self)
        StartJoystickOp.timer = context.window_manager.event_timer_add(motion_control.joystick.JoystickControl.POLL_INTERVAL, context.window)

        ForceRedraw()
        return {'RUNNING_MODAL'}

    def cancel(self, context):
        StartJoystickOp.stop(context, False)
        return {'CANCELLED'}


class StopJoystickOp(bpy.types.Operator):
    """Start interactive rig control"""
    bl_idname = 'motion_control.stop_joystick'
    bl_label = 'Stop'

    @classmethod
    def poll(cls, context):
        return (bpy.data.objects.find('Robot') != -1) and (StartJoystickOp.joystick is not None)

    def execute(self, context):
        StartJoystickOp.stop(context, False)
        return {'FINISHED'}


class ExportOp(bpy.types.Operator, ExportHelper):
    """Export motion"""
    bl_idname = 'motion_control.export'
    bl_label = 'Export Motion'
    filename_ext = '.cam'

    serial_ports = []

    def get_serial_port_enum(self):
        return 1

    def set_serial_port_enum(self, value):
        print("setting serial port to", value)

    def update_func(self, context):
        print("my test function", context)

    serial_ports = get_serial_ports()

    write_extchan_prog = bpy.props.BoolProperty(name='Export External Channels',
                                                description='Export program for external channel controller',
                                                default=False)
    write_extchan_serial_port = bpy.props.EnumProperty(name='Box Serial Port',
                                                       description='External Box Serial Port',
                                                       items=serial_ports)

    @classmethod
    def poll(cls, context):
        return bpy.data.objects.find('Robot') != -1

    def execute(self, context):
        write_extchan = {
            "do_prog": self.write_extchan_prog,
            "serial_port": self.write_extchan_serial_port,
        }
        Export(self.filepath, write_extchan)
        return {'FINISHED'}

class ExportCameraKeysOp(bpy.types.Operator, ExportHelper):
    """Export camera keys"""
    bl_idname = 'motion_control.export_cam_keys'
    bl_label = 'Export Camera Keys'
    filename_ext = '.keys';
    
    @classmethod
    def poll(cls, context):
        return (bpy.data.objects.find('Robot') != -1)

    def execute(self, context):
        ExportCameraKeys(self.filepath)
        return {'FINISHED'}
    
    def draw(self, context):
        layout = self.layout
        
        row = layout.row()
        row.prop(self, 'full_range_prop')


class MainPanel(bpy.types.Panel):
    bl_label = 'Camera Motion Control'
    bl_idname = 'SCENE_PT_CamMotionControl'
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = 'scene'

    def draw(self, context):
        layout = self.layout

        if bpy.data.objects.find('Robot') == -1:
            row = layout.row()
            row.scale_y = 2.0
            row.operator('motion_control.import_rig')
            return

        row = layout.row()
        row.operator('motion_control.select_pos')
        row.operator('motion_control.select_target')

        row = layout.row()
        row.label('Joystick Control:')
        row.operator('motion_control.start_joystick')
        row.operator('motion_control.stop_joystick')

        row = layout.row()
        row.scale_y = 2.0
        row.operator('motion_control.export')
        row.operator('motion_control.export_cam_keys')
        row = layout.row()
        row.scale_y = 2.0
        row.operator('motion_control.import_cam_anim')

        
class TriggerPropItem(bpy.types.PropertyGroup):
    name = bpy.props.StringProperty(name = 'Name')
    frame = bpy.props.IntProperty(name = 'Frame')


class RigProperties(bpy.types.PropertyGroup):
    NUM_EXTERNAL_CHANS = 8

    __elbowcfg_desc = [ ('above', 'Above', 'Above'), ('below', 'Below', 'Below') ]
    __attachment_desc = [ ('top', 'Top', 'Top'), ('left', 'Left', 'Left'), ('right', 'Right', 'Right') ]
    __joymode_desc = [ ('world', 'World', 'World'), ('camera', 'Camera', 'Camera') ]
    __joyselection_desc = [ ('position', 'Position', 'Position'), ('target', 'Target', 'Target') ]
    elbow_cfg = bpy.props.EnumProperty(name = 'Elbow Config', items = __elbowcfg_desc)
    cage_attachment = bpy.props.EnumProperty(name = 'Cage Attachment', items = __attachment_desc)
    cage_spacer = bpy.props.FloatProperty(name = 'Cage Spacer', subtype='DISTANCE', unit='LENGTH')
    cam_pivot = bpy.props.FloatVectorProperty(name = 'Pivot', subtype='TRANSLATION', unit='LENGTH')
    camera_depth = bpy.props.FloatProperty(name = 'Camera Depth', default = 0.3, subtype='DISTANCE', unit='LENGTH')
    lens_depth = bpy.props.FloatProperty(name = 'Lens Depth', default = 0.25, subtype='DISTANCE', unit='LENGTH')
    riser_height = bpy.props.FloatProperty(name = 'Riser', default = 0.77, subtype='DISTANCE', unit='LENGTH')
    platform_height = bpy.props.FloatProperty(name = 'Platform Height', default = 0.24, subtype='DISTANCE', unit='LENGTH')
    platform_radius = bpy.props.FloatProperty(name = 'Platform Radius', default = 0.6, subtype='DISTANCE', unit='LENGTH')
    cam_roll = bpy.props.FloatProperty(name = 'Roll', subtype='ANGLE', unit='ROTATION')
    joy_mode = bpy.props.EnumProperty(name = 'Joystick Mode', items = __joymode_desc)
    joy_selection = bpy.props.EnumProperty(name = 'Joystick Selection', items = __joyselection_desc)
    triggers = bpy.props.CollectionProperty(name = 'Triggers', type = TriggerPropItem)
    active_trigger = bpy.props.IntProperty(name = 'Active Trigger')
    extern_chan_0_ena = bpy.props.BoolProperty(name='')
    extern_chan_0_name = bpy.props.StringProperty(name='')
    extern_chan_0_val = bpy.props.FloatProperty(name='', min=0, max=10000)
    extern_chan_1_ena = bpy.props.BoolProperty(name='')
    extern_chan_1_name = bpy.props.StringProperty(name='')
    extern_chan_1_val = bpy.props.FloatProperty(name='', min=0, max=10000)
    extern_chan_2_ena = bpy.props.BoolProperty(name='')
    extern_chan_2_name = bpy.props.StringProperty(name='')
    extern_chan_2_val = bpy.props.FloatProperty(name='', min=0, max=10000)
    extern_chan_3_ena = bpy.props.BoolProperty(name='')
    extern_chan_3_name = bpy.props.StringProperty(name='')
    extern_chan_3_val = bpy.props.FloatProperty(name='', min=0, max=10000)
    extern_chan_4_ena = bpy.props.BoolProperty(name='')
    extern_chan_4_name = bpy.props.StringProperty(name='')
    extern_chan_4_val = bpy.props.FloatProperty(name='', min=0, max=10000)
    extern_chan_5_ena = bpy.props.BoolProperty(name='')
    extern_chan_5_name = bpy.props.StringProperty(name='')
    extern_chan_5_val = bpy.props.FloatProperty(name='', min=0, max=10000)
    extern_chan_6_ena = bpy.props.BoolProperty(name='')
    extern_chan_6_name = bpy.props.StringProperty(name='')
    extern_chan_6_val = bpy.props.FloatProperty(name='', min=0, max=10000)
    extern_chan_7_ena = bpy.props.BoolProperty(name='')
    extern_chan_7_name = bpy.props.StringProperty(name='')
    extern_chan_7_val = bpy.props.FloatProperty(name='', min=0, max=10000)


class AddCameraPreset(bl_operators.presets.AddPresetBase, bpy.types.Operator):
    bl_idname = 'motion_control.cam_preset_add'
    bl_label = 'Add Camera Preset'
    preset_menu = 'MotionControlCamPresets'

    preset_defines = [
        "robot = bpy.data.objects['Robot']"
    ]

    preset_values = [
        'robot.rig_properties.cam_pivot',
        'robot.rig_properties.camera_depth'
    ]

    preset_subdir = 'motion_control_cams'


class MotionControlCamPresets(bpy.types.Menu):
    bl_label = 'Camera Presets'
    preset_subdir = 'motion_control_cams'
    preset_operator = 'script.execute_preset'
    draw = bpy.types.Menu.draw_preset


class AddLensPreset(bl_operators.presets.AddPresetBase, bpy.types.Operator):
    bl_idname = 'motion_control.lens_preset_add'
    bl_label = 'Add Lens Preset'
    preset_menu = 'MotionControlLensPresets'

    preset_defines = [
        "robot = bpy.data.objects['Robot']",
        "drivenCam = bpy.data.objects['DrivenCamera']"
    ]

    preset_values = [
        'robot.rig_properties.lens_depth',
        'drivenCam.data.sensor_width',
        'drivenCam.data.sensor_height',
        'drivenCam.data.sensor_fit'
    ]

    preset_subdir = 'motion_control_lenses'


class MotionControlLensPresets(bpy.types.Menu):
    bl_label = 'Lens Presets'
    preset_subdir = 'motion_control_lenses'
    preset_operator = 'script.execute_preset'
    draw = bpy.types.Menu.draw_preset


def sortTriggers(triggers):
    numTriggers = len(triggers)
    for i in range(numTriggers - 1):
        for j in range(i + 1, numTriggers):
            if triggers[i].frame > triggers[j].frame:
                triggers[i].frame, triggers[j].frame = triggers[j].frame, triggers[i].frame
                triggers[i].name, triggers[j].name = triggers[j].name, triggers[i].name


class AddTriggerOp(bpy.types.Operator):
    """Add a trigger"""
    bl_idname = 'motion_control.trigger_add'
    bl_label = 'Add Trigger'
    name = StringProperty(name = 'Name')
    frame = IntProperty(name = 'Frame')

    @classmethod
    def poll(cls, context):
        return (bpy.data.objects.find('Robot') != -1)

    def execute(self, context):
        robot = bpy.data.objects['Robot']
        triggers = robot.rig_properties.triggers

        newTrigger = triggers.add()
        newTrigger.name = self.name
        newTrigger.frame = self.frame
        sortTriggers(triggers)

        return {'FINISHED'}
 
    def invoke(self, context, event):
        robot = bpy.data.objects['Robot']
        defaultIdx = len(robot.rig_properties.triggers)
        self.name = 'Trigger ' + str(defaultIdx)
        self.frame = bpy.context.scene.frame_current
        wm = context.window_manager
        return wm.invoke_props_dialog(self)

    def draw(self, context):
        layout = self.layout

        column = layout.column()
        column.prop(self, 'name')
        column.prop(self, 'frame')


class RemoveTriggerOp(bpy.types.Operator):
    """Add a trigger"""
    bl_idname = 'motion_control.trigger_remove'
    bl_label = 'Remove Trigger'

    @classmethod
    def poll(cls, context):
        if bpy.data.objects.find('Robot') == -1:
            return False
        robot = bpy.data.objects['Robot']
        return (robot.rig_properties.active_trigger >= 0) and (robot.rig_properties.active_trigger < len(robot.rig_properties.triggers))
 
    def execute(self, context):
        robot = bpy.data.objects['Robot']
        selected = robot.rig_properties.active_trigger
        if (selected >= 0) and (selected < len(robot.rig_properties.triggers)):
            robot.rig_properties.triggers.remove(selected)
            if selected > 0:
                robot.rig_properties.active_trigger -= 1

        return {'FINISHED'}


class EditTriggerOp(bpy.types.Operator):
    """Add a trigger"""
    bl_idname = 'motion_control.trigger_edit'
    bl_label = 'Edit Trigger'
    name = StringProperty(name = 'Name')
    frame = IntProperty(name = 'Frame')

    @classmethod
    def poll(cls, context):
        if bpy.data.objects.find('Robot') == -1:
            return False
        robot = bpy.data.objects['Robot']
        return (robot.rig_properties.active_trigger >= 0) and (robot.rig_properties.active_trigger < len(robot.rig_properties.triggers))

    def execute(self, context):
        robot = bpy.data.objects['Robot']
        triggers = robot.rig_properties.triggers
        selected = robot.rig_properties.active_trigger

        if (selected < 0) or (selected >= len(triggers)):
            return

        editTrigger = triggers[selected]
        editTrigger.name = self.name
        editTrigger.frame = self.frame
        sortTriggers(triggers)

        return {'FINISHED'}

    def invoke(self, context, event):
        robot = bpy.data.objects['Robot']

        selected = robot.rig_properties.active_trigger
        if (selected < 0) or (selected >= len(robot.rig_properties.triggers)):
            return

        trigger = robot.rig_properties.triggers[selected]
        self.name = trigger.name
        self.frame = trigger.frame
        wm = context.window_manager
        return wm.invoke_props_dialog(self)


class TriggerList(bpy.types.UIList):
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):
        frameText = 'Frame# ' + str(item.frame)
        nameText = item.name

        split = layout.split(0.2)
        split.label(text = frameText, translate=False)
        split.label(text = nameText, translate=False, icon_value=icon)


class RigPropertiesPanel(bpy.types.Panel):
    bl_label = 'Motion Control Rig'
    bl_idname = 'SCENE_PT_MotionControlRig'
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = 'object'

    @classmethod
    def poll(cls, context):
        return (context.object is not None) and (context.object.name == 'Robot')

    def draw(self, context):
        layout = self.layout
        obj = context.object

        row = layout.row()
        row.prop(obj.rig_properties, 'elbow_cfg')

        row = layout.row()
        row.prop(obj.rig_properties, 'cage_attachment')
        row.prop(obj.rig_properties, 'cage_spacer')
        row.prop(obj.rig_properties, 'cam_roll')

        column = layout.column()
        column.label(text = 'Triggers:')
        column.template_list('TriggerList', 'robot_rig_triggers', obj.rig_properties, 'triggers', obj.rig_properties, 'active_trigger')
        row = column.row()
        row.operator('motion_control.trigger_add')
        row.operator('motion_control.trigger_remove')
        row.operator('motion_control.trigger_edit')

        row = layout.row(align=True)
        row.menu('MotionControlCamPresets', text = 'Camera Presets')
        row.operator('motion_control.cam_preset_add', text='', icon='ZOOMIN')
        row.operator('motion_control.cam_preset_add', text='', icon='ZOOMOUT').remove_active = True
        row.menu('MotionControlLensPresets', text = 'Lens Presets')
        row.operator('motion_control.lens_preset_add', text='', icon='ZOOMIN')
        row.operator('motion_control.lens_preset_add', text='', icon='ZOOMOUT').remove_active = True

        row = layout.row()
        row.prop(obj.rig_properties, 'cam_pivot')

        row = layout.row()
        row.prop(obj.rig_properties, 'joy_mode')
        row.prop(obj.rig_properties, 'joy_selection')

        row = layout.row()
        row.prop(obj.rig_properties, 'camera_depth')
        row.prop(obj.rig_properties, 'lens_depth')

        row = layout.row()
        row.prop(obj.rig_properties, 'riser_height')

        row = layout.row()
        row.prop(obj.rig_properties, 'platform_height')
        row.prop(obj.rig_properties, 'platform_radius')

        layout.label(text = 'External Channels:')

        for i in range(RigProperties.NUM_EXTERNAL_CHANS):
            row = layout.row(align=True)
            row.prop(obj.rig_properties, 'extern_chan_%d_ena' % i, text='')
            grp = row.row(align=True)
            grp.prop(obj.rig_properties, 'extern_chan_%d_name' % i, text='')
            grp.prop(obj.rig_properties, 'extern_chan_%d_val' % i, text='')
            grp.active = getattr(obj.rig_properties, 'extern_chan_%d_ena' % i)


class MessageOperator(bpy.types.Operator):
    bl_idname = "error.message"
    bl_label = "Message"
    message = StringProperty()
 
    def execute(self, context):
        self.report({'WARNING'}, self.message)
        return {'FINISHED'}
 
    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self, width=600, height=200)
 
    def draw(self, context):
        self.layout.label("ERROR Message")
        row = self.layout.split(1)
        row.prop(self, "message")
