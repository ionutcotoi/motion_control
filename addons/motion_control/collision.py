#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import platform
import ctypes

g_nativeLib = None
g_checkFunc = None
g_shutdownFunc = None

def init():
    global g_nativeLib
    global g_checkFunc
    global g_shutdownFunc

    modPath = os.path.abspath(os.path.dirname(__file__))
    osName = platform.system()
    osBits = platform.architecture()[0]

    try:
        if osName == 'Windows':
            if osBits == '64bit':
                dllName = 'collision_win64.dll'
            else:
                dllName = 'collision_win32.dll'
            libPath = os.path.join(modPath, dllName)
            g_nativeLib = ctypes.WinDLL(libPath)
        else:
            if osName == 'Darwin':
                soName = 'collision_osx.so'
            else:
                soName = 'collision_linux.so'
            libPath = os.path.join(modPath, soName)
            g_nativeLib = ctypes.CDLL(libPath, mode=ctypes.RTLD_LOCAL)
    except OSError:
        print('Could not load collision library from "' + libPath + '".')
        return False

    try:
        if osName == 'Windows':
            initProto = ctypes.WINFUNCTYPE(ctypes.c_int)
            shutdownProto = ctypes.WINFUNCTYPE(None)
            checkProto = ctypes.WINFUNCTYPE(ctypes.c_int, ctypes.c_void_p, ctypes.c_float, ctypes.c_int, ctypes.c_float, ctypes.c_float, ctypes.c_float, ctypes.c_float, ctypes.c_float)
        else:
            initProto = ctypes.CFUNCTYPE(ctypes.c_int)
            shutdownProto = ctypes.CFUNCTYPE(None)
            checkProto = ctypes.CFUNCTYPE(ctypes.c_int, ctypes.c_void_p, ctypes.c_float, ctypes.c_int, ctypes.c_float, ctypes.c_float, ctypes.c_float, ctypes.c_float, ctypes.c_float)
        initFunc = initProto(('CollisionInit', g_nativeLib))
        g_shutdownFunc = shutdownProto(('CollisionShutdown', g_nativeLib))
        g_checkFunc = checkProto(('CollisionCheck', g_nativeLib))
    except AttributeError:
        print('Cannot find the required functions in "' + libPath + '"')
        return False

    if not initFunc():
        return False

    return True

def shutdown():
    global g_nativeLib
    global g_checkFunc
    global g_shutdownFunc

    g_checkFunc = None

    if g_shutdownFunc is not None:
        g_shutdownFunc()
        g_shutdownFunc = None

    if g_nativeLib is not None:
        del g_nativeLib
        g_nativeLib = None

def check(angles, cageSpacer, cageAttachment, cameraDepth, lensDepth, riserHeight, platformHeight, platformRadius):
    if g_checkFunc is None:
        return False

    anglesArr = (ctypes.c_float * len(angles))(*angles)
    return g_checkFunc(anglesArr, cageSpacer, cageAttachment, cameraDepth, lensDepth, riserHeight, platformHeight, platformRadius)
