#!/usr/bin/python
# -*- coding: utf-8 -*-

import math
import bpy
import mathutils
import motion_control.collision

g_shoulderPivot = None
g_armLen = None
g_forearmLen = None
g_wristLen = None
g_flangeBoneLen = None

# I tried to get the bounding box of the cage from the scene, but it didn't work. cage.bound_box.data.dimensions
# is supposed to give you object-space dimensions, but for some reason they change as the object moves
# (probably due to skinning). Did I mention that Blender is retarded?
g_cageSize = mathutils.Vector((0.10, 0.18, 0.19))
g_cageWallHeight = 0.01

# These are in radians. See page 33 in RX130B_INSTRUCTION_EN.pdf.
g_jointAngleLimits = [
    (math.radians(-160.0), math.radians(160.0)),
    (math.radians(-137.5), math.radians(137.5)),
    (math.radians(-142.5), math.radians(142.5)),
    (math.radians(-270.0), math.radians(270.0)),
    (math.radians(-105.0), math.radians(120.0)),
    (math.radians(-270.0), math.radians(270.0))
]

# These are in radians/s. See page 33 in RX130B_INSTRUCTION_EN.pdf.
g_jointMaxSpeeds = [
    math.radians(278),
    math.radians(278),
    math.radians(356),
    math.radians(409),
    math.radians(800),
    math.radians(1125)
]

# These are nominal speeds, also in radians/s.
g_jointNomSpeeds = [
    math.radians(203),
    math.radians(185),
    math.radians(250),
    math.radians(400),
    math.radians(320),
    math.radians(580)
]

def initGlobals():
    global g_shoulderPivot
    global g_armLen
    global g_forearmLen
    global g_wristLen
    global g_flangeBoneLen

    if g_shoulderPivot is not None:
        return

    bones = bpy.data.objects['Robot'].data.bones
    shoulderBone = bones['shoulderBone']
    armBone = bones['armBone']
    forearmBone = bones['forearmBone']
    wristBone = bones['wristBone']
    flangeBone = bones['flangeBone']

    shoulderHeight = shoulderBone.head_local[2]
    g_shoulderPivot = mathutils.Vector((0, 0, shoulderHeight))
    g_armLen = armBone.tail_local[2] - armBone.head_local[2]
    g_forearmLen = forearmBone.tail_local[2] - forearmBone.head_local[2]
    g_wristLen = wristBone.tail_local[2] - wristBone.head_local[2]
    g_flangeBoneLen = flangeBone.tail_local[2] - flangeBone.head_local[2]

    motion_control.collision.init()

def angleBetween(v1, v2, n):
    """
    Returns the angle between the vectors v1 and v2. The angle is positive if the normal of the
    plane determined by the vectors points in the same direction as the passed normal (n),
    negative otherwise.
    """
    c = v1.cross(v2)
    a = math.atan2(c.length, v1.dot(v2))
    if n.dot(c) < 0:
        a = -a
    return a

def orientCamPivot(camPivot, cageAttachment):
    if cageAttachment == 'left':
        return mathutils.Vector((g_cageSize.z * 0.5 - g_cageWallHeight - camPivot.z, camPivot.y,  g_cageSize.y * 0.5 - camPivot.x))
    elif cageAttachment == 'right':
        return mathutils.Vector((-g_cageSize.z * 0.5 + g_cageWallHeight + camPivot.z, camPivot.y,  g_cageSize.y * 0.5 + camPivot.x))
    else:
        return mathutils.Vector((camPivot.x, camPivot.y,  g_cageSize.y - camPivot.z))

def getRigControls():
    posLocator = bpy.data.objects['Camera_Position']
    camPos = posLocator.matrix_world.to_translation()
    targetLocator = bpy.data.objects['Camera_Target']
    camTarget = targetLocator.matrix_world.to_translation()
    roll = bpy.data.objects['Robot'].rig_properties.cam_roll
    polePos = bpy.data.objects['Camera_Pole'].matrix_world.to_translation() - camPos
    return (camPos, camTarget, polePos, roll)

def computeCamAxes(camPos, camTarget, poleVect, roll):
    forwardDir = (camTarget - camPos).normalized()
    poleVect = poleVect.normalized()

    dot = math.fabs(forwardDir.dot(poleVect))
    if dot > 0.99:
        # If the pole vector coincides with the forward vector, try world up.
        poleVect = mathutils.Vector((0, 0, 1))
        dot = math.fabs(forwardDir.dot(poleVect))
        if dot > 0.99:
            # The pole vector was the same as world up already. Use negative X, which is as good
            # a choice as anything else.
            poleVect = mathutils.Vector((-1, 0, 0))

    # Rotate the pole vector around the forward direction to obtain the camera up vector.
    forwardRotMat = mathutils.Matrix.Rotation(roll, 3, forwardDir)
    upDir = forwardRotMat * poleVect

    # Compute the right direction, then cross with forward to compute up again. This makes sure that
    # the resulting base is orthonormal.
    rightDir = forwardDir.cross(upDir).normalized()
    upDir = forwardDir.cross(rightDir).normalized()

    assert(upDir.length > 0.1)
    return (rightDir, forwardDir, upDir)

def getCameraSpace():
    camPos, camTarget, poleVect, roll = getRigControls()
    return computeCamAxes(camPos, camTarget, poleVect, roll)

def solveRobot(camPosX, camPosY, camPosZ, targetPosX, targetPosY, targetPosZ, rollAngle = None, polePos = None):
    initGlobals()
    robot = bpy.data.objects['Robot']
    invWM = robot.matrix_world.inverted()

    elbowCfg = robot.rig_properties.elbow_cfg
    cageAttachment = robot.rig_properties.cage_attachment
    cageSpacer = robot.rig_properties.cage_spacer
    camPivot = orientCamPivot(robot.rig_properties.cam_pivot, cageAttachment)
    if rollAngle is None:
        rollAngle = robot.rig_properties.cam_roll
    camDepth = robot.rig_properties.camera_depth
    lensDepth = robot.rig_properties.lens_depth
    riserHeight = robot.rig_properties.riser_height
    platformHeight = robot.rig_properties.platform_height
    platformRadius = robot.rig_properties.platform_radius

    if cageAttachment == 'left':
        rollOffset = math.pi * 0.5
        cageAttachmentInt = 1
    elif cageAttachment == 'right':
        rollOffset = -math.pi * 0.5
        cageAttachmentInt = 2
    else:
        rollOffset = 0
        cageAttachmentInt = 0

    xAxis = mathutils.Vector((1, 0, 0))
    yAxis = mathutils.Vector((0, 1, 0))
    zAxis = mathutils.Vector((0, 0, 1))

    rollAngle = rollAngle + rollOffset
    camPos = invWM * mathutils.Vector((camPosX, camPosY, camPosZ))
    targetPos = invWM * mathutils.Vector((targetPosX, targetPosY, targetPosZ))

    if polePos is None:
        if bpy.data.objects.find('Camera_Pole') != -1:
            polePos = bpy.data.objects['Camera_Pole'].matrix_world.to_translation()
            polePos = polePos - mathutils.Vector((camPosX, camPosY, camPosZ))
        else:
            polePos = mathutils.Vector((0, 0, 1))

    poleVect = polePos.normalized()

    rightDir, forwardDir, wristDir = computeCamAxes(camPos, targetPos, poleVect, rollAngle)

    # The cage spacer size and height offset must be applied along the wrist direction. The depth offset
    # must be applied in the forward direction. The side offset must be applied in the remaining
    # perpendicular direction.
    flangePos = camPos - wristDir*(cageSpacer + camPivot.z) - forwardDir*camPivot.y - rightDir*camPivot.x
    wristPos = flangePos - wristDir * g_wristLen

    # Rotate the shoulder so that the vector from the origin to the wrist position becomes the robot's
    # forward direction.
    shoulderAngle = math.atan2(wristPos[1], wristPos[0])

    # Put the positions in "arm space" by applying the shoulder rotation and subtracting the shoulder pivot.
    shoulderRotMat = mathutils.Matrix.Rotation(-shoulderAngle, 3, 'Z')
    localWristPos = shoulderRotMat * wristPos - g_shoulderPivot
    localFlangePos = shoulderRotMat * flangePos - g_shoulderPivot
    localCamPos = shoulderRotMat * camPos - g_shoulderPivot
    localTargetPos = shoulderRotMat * targetPos - g_shoulderPivot

    # Compute the rotation which aligns the arm with the wrist position vector.
    elevation = math.atan2(localWristPos[2], localWristPos[0])

    # Solve the angles of the triangle made by the arm, forearm and target vector.
    wristPosDist = localWristPos.length
    if (wristPosDist > 0) and (g_armLen + g_forearmLen >= wristPosDist):
        reachable = True

        wristPosDistSq = wristPosDist * wristPosDist
        armLenSq = g_armLen * g_armLen
        forearmLenSq = g_forearmLen * g_forearmLen

        armCos = -(forearmLenSq - armLenSq - wristPosDistSq) / (2 * g_armLen * wristPosDist)
        armAngle = elevation + math.acos(armCos)

        elbowCos = -(wristPosDistSq - armLenSq - forearmLenSq) / (2 * g_armLen * g_forearmLen)
        elbowAngle = math.acos(elbowCos)

        if elbowCfg == 'below':
            armAngle = elevation - math.acos(armCos)
            elbowAngle = 2*math.pi - elbowAngle
    else:
        armAngle = elevation
        elbowAngle = math.pi
        if math.fabs(g_armLen + g_forearmLen - wristPosDist) < 0.000001:
            reachable = True
        else:
            reachable = False

    # We need to rotate the forearm so that wrist joint rotates in the plane determined by the elbow,
    # wrist and flange position. This is the angle between the normal of said plane and the Y axis.
    armRotMat = mathutils.Matrix.Rotation(-armAngle, 3, 'Y')
    localElbowPos = armRotMat * mathutils.Vector((g_armLen, 0, 0))
    localForearmVect = localWristPos - localElbowPos
    tmp = localFlangePos - localElbowPos
    n = localForearmVect.cross(tmp).normalized()
    forearmAngle = angleBetween(yAxis, n, localForearmVect)

    # Rotate the flange position onto the wrist rotation plane, then obtain the angle between the resulting wrist vector
    # and the horizontal.
    wristVect = localFlangePos - localWristPos
    forearmRotMat = mathutils.Matrix.Rotation(-forearmAngle, 3, localForearmVect)
    wristVect = forearmRotMat * wristVect
    wristAngle = angleBetween(xAxis, wristVect, yAxis)

    # Put the target vector in the space of the flange joint to compute the flange rotation. The forward
    # direction is the Z axis in flange space.
    targetVect = localTargetPos - localCamPos
    targetVect = forearmRotMat * targetVect
    wristRotMat = mathutils.Matrix.Rotation(-wristAngle, 3, yAxis)
    targetVect = wristRotMat * targetVect
    flangeAngle = angleBetween(zAxis, targetVect, xAxis)

    # To obtain the final wrist angle, take into account that the arm positioning introduces an extra
    # rotation equal to the angle between the forearm and the horizontal.
    wristOffset = (math.pi - armAngle - elbowAngle)
    wristAngle = wristAngle - wristOffset

    # Compensate for the initial rotation (at rest, the arm is vertical).
    armAngle = -(armAngle - math.pi/2)
    elbowAngle = math.pi - elbowAngle

    # Apply limits.
    angles = [shoulderAngle, armAngle, elbowAngle, forearmAngle, wristAngle, flangeAngle]
    for i in range(len(angles)):
        if angles[i] < g_jointAngleLimits[i][0]:
            angles[i] = g_jointAngleLimits[i][0]
            reachable = False
        elif angles[i] > g_jointAngleLimits[i][1]:
            angles[i] = g_jointAngleLimits[i][1]
            reachable = False

    if reachable:
        if motion_control.collision.check(angles, cageSpacer, cageAttachmentInt, camDepth, lensDepth, riserHeight, platformHeight, platformRadius):
            reachable = False

    #print([math.degrees(x) for x in angles])
    return angles + [reachable]

def solveShoulder(camPosX, camPosY, camPosZ, targetPosX, targetPosY, targetPosZ):
    return solveRobot(camPosX, camPosY, camPosZ, targetPosX, targetPosY, targetPosZ)[0]

def solveArm(camPosX, camPosY, camPosZ, targetPosX, targetPosY, targetPosZ):
    return solveRobot(camPosX, camPosY, camPosZ, targetPosX, targetPosY, targetPosZ)[1]

def solveElbow(camPosX, camPosY, camPosZ, targetPosX, targetPosY, targetPosZ):
    return solveRobot(camPosX, camPosY, camPosZ, targetPosX, targetPosY, targetPosZ)[2]

def solveForearm(camPosX, camPosY, camPosZ, targetPosX, targetPosY, targetPosZ):
    return solveRobot(camPosX, camPosY, camPosZ, targetPosX, targetPosY, targetPosZ)[3]

def solveWrist(camPosX, camPosY, camPosZ, targetPosX, targetPosY, targetPosZ):
    return solveRobot(camPosX, camPosY, camPosZ, targetPosX, targetPosY, targetPosZ)[4]

def solveFlange(camPosX, camPosY, camPosZ, targetPosX, targetPosY, targetPosZ):
    return solveRobot(camPosX, camPosY, camPosZ, targetPosX, targetPosY, targetPosZ)[5]

def solveReachability(camPosX, camPosY, camPosZ, targetPosX, targetPosY, targetPosZ):
    reachable = solveRobot(camPosX, camPosY, camPosZ, targetPosX, targetPosY, targetPosZ)[6]
    if reachable:
        return 0
    else:
        return 1

def getCagePosition():
    initGlobals()
    robot = bpy.data.objects['Robot']
    cageSpacer = robot.rig_properties.cage_spacer
    cageAttachment = robot.rig_properties.cage_attachment

    x = 0
    z = g_shoulderPivot.z + g_armLen + g_forearmLen + g_wristLen + cageSpacer

    if cageAttachment == 'left':
        y = g_cageSize.z * 0.5
        z += g_cageSize.y * 0.5
    elif cageAttachment == 'right':
        y = -g_cageSize.z * 0.5
        z += g_cageSize.y * 0.5
    else:
        y = 0

    return mathutils.Vector((x, y, z))

def getCageRotation():
    robot = bpy.data.objects['Robot']
    cageAttachment = robot.rig_properties.cage_attachment
    if cageAttachment == 'left':
        return mathutils.Vector((0, 0, 0))
    elif cageAttachment == 'right':
        return mathutils.Vector((math.pi, 0, 0))
    else:
        return mathutils.Vector((-math.pi * 0.5, 0, 0))

def getDrivenCamPosition(cageSpacer, camPivotX, camPivotY, camPivotZ):
    initGlobals()

    robot = bpy.data.objects['Robot']
    cageAttachment = robot.rig_properties.cage_attachment
    camPivot = orientCamPivot(mathutils.Vector((camPivotX, camPivotY, camPivotZ)), cageAttachment)

    # The axes are swapped because we need a weird rotation to align the camera properly at rest. Also,
    # we need to offset the height by the length of the flange bone, since the camera is parented to that,
    # which means it's attached to the tip of the bone.
    x = -camPivot.y
    y = cageSpacer - g_flangeBoneLen + camPivot.z
    z = camPivot.x
    return mathutils.Vector((x, y, z))

def getDrivenCamRotation():
    robot = bpy.data.objects['Robot']
    cageAttachment = robot.rig_properties.cage_attachment
    if cageAttachment == 'left':
        return mathutils.Vector((math.pi * 0.5, math.pi, math.pi * 0.5))
    elif cageAttachment == 'right':
        return mathutils.Vector((math.pi * 0.5, 0, math.pi * 0.5))
    else:
        return mathutils.Vector((math.pi * 0.5, -math.pi * 0.5, math.pi * 0.5))

def getCameraBboxTransform():
    robot = bpy.data.objects['Robot']

    camDepth = robot.rig_properties.camera_depth
    lensDepth = robot.rig_properties.lens_depth
    cageSpacer = robot.rig_properties.cage_spacer
    
    xScale = 0.5 * (camDepth + lensDepth)
    xOffset = xScale - lensDepth
    yOffset = -g_flangeBoneLen + g_cageWallHeight + cageSpacer

    return (xScale, xOffset, yOffset)

def computeFK(jointAngles):
    yAxis = mathutils.Vector((0, 1, 0))
    zAxis = mathutils.Vector((0, 0, 1))
    boneHeights = [ 0.550, 0.000, 0.625, 0.000, 0.625, 0.110 ]
    boneAxes = [ zAxis, yAxis, yAxis, zAxis, yAxis, zAxis ]

    xform = mathutils.Matrix.Translation((0, 0, 0))
    for i in range(6):
        jointAngle = math.radians(jointAngles[i])
        boneRot = mathutils.Matrix.Rotation(jointAngle, 4, boneAxes[i])
        boneTrans = mathutils.Matrix.Translation((0, 0, boneHeights[i]))
        boneXform = boneTrans * boneRot
        xform = xform * boneXform

    loc, rotQ, _ = xform.decompose()
    return (loc, rotQ)

def getEffectorPoint(jointAngles):
    loc, rotQ = computeFK(jointAngles)

    # We need the point in millimeters. Also, the robot world space is relative to the shoulder, not the base.
    loc[2] = loc[2] - g_shoulderPivot[2]
    loc = [ '%.6f' % (x * 1000) for x in loc ]

    # We need the rotations in degrees. The rotation order is X, Y, Z, which in Blender's world is called 'ZYX'.
    angles = [ math.degrees(x) for x in rotQ.to_euler('ZYX')]
    rot = [ '%.6f' % x for x in angles ]

    shoulderCfg = 'lefty'
    elbowCfg = 'epositive' if jointAngles[2] >= 0 else 'enegative'
    wristCfg = 'wpositive' if jointAngles[4] >= 0 else 'wnegative'

    return (loc, rot, (shoulderCfg, elbowCfg, wristCfg))

def controlsFromAngles(jointAngles):
    rootLocator = bpy.data.objects['Robot_Root']
    robot = bpy.data.objects['Robot']
    posLocator = bpy.data.objects['Camera_Position']
    targetLocator = bpy.data.objects['Camera_Target']
    poleLocator = bpy.data.objects['Camera_Pole']
    
    cageSpacer = robot.rig_properties.cage_spacer
    cageAttachment = robot.rig_properties.cage_attachment
    camPivot = orientCamPivot(robot.rig_properties.cam_pivot, cageAttachment)
    riserHeight = robot.rig_properties.riser_height
    platformHeight = robot.rig_properties.platform_height

    crPos = posLocator.matrix_world.to_translation()
    crTarget = targetLocator.matrix_world.to_translation()
    targetDist = (crTarget - crPos).length

    crPole = poleLocator.matrix_world.to_translation()
    poleDist = (crPole - crPos).length

    crRoll = robot.rig_properties.cam_roll

    flangePos, flangeRotQ = computeFK(jointAngles)
    flangeRotMat = flangeRotQ.to_matrix()

    flangeRotTransp = flangeRotMat.transposed()
    camX = -flangeRotTransp[0].normalized()
    camY = -flangeRotTransp[1].normalized()
    camZ = -flangeRotTransp[2].normalized()

    camPos = flangePos - camZ*(cageSpacer + camPivot.z) + camX*camPivot.y + camY*camPivot.x
    camPos.z += riserHeight + platformHeight

    camLocatorPos = rootLocator.matrix_world * camPos
    targetLocatorPos = rootLocator.matrix_world * (camPos + camX * targetDist)

    if cageAttachment == 'left':
        poleOffsetRot = math.pi * 0.5
    elif cageAttachment == 'right':
        poleOffsetRot = -math.pi * 0.5
    else:
        poleOffsetRot = 0

    poleOffsetRot = crRoll + poleOffsetRot
    forwardRotMat = mathutils.Matrix.Rotation(-poleOffsetRot, 3, camX)
    poleLocatorPos = forwardRotMat * (camZ * poleDist)

    return (camLocatorPos, targetLocatorPos, poleLocatorPos, crRoll)

def applyControlValues(camLocatorPos, targetLocatorPos, poleLocatorPos, roll):
    robot = bpy.data.objects['Robot']
    posLocator = bpy.data.objects['Camera_Position']
    targetLocator = bpy.data.objects['Camera_Target']
    poleLocator = bpy.data.objects['Camera_Pole']

    posLocator.location = camLocatorPos
    targetLocator.location = targetLocatorPos
    poleLocator.location = poleLocatorPos
    robot.rig_properties.cam_roll = roll

def applyAngles(jointAngles):
    camLocatorPos, targetLocatorPos, poleLocatorPos, roll = controlsFromAngles(jointAngles)
    applyControlValues(camLocatorPos, targetLocatorPos, poleLocatorPos, roll)

# Take angle in degrees and map it to [0..360)
def NormalizeAngle(a):
    while a < 0:
        a = a + 360
    return a % 360

def ComplimentaryRot(a):
    if a > 0:
        return a - 360
    else:
        return 360 + a

# Return the two possible deltas which take us from angle a to b (shortest first).
def AngleDelta(a, b):
    delta = NormalizeAngle(b) - NormalizeAngle(a)
    if abs(delta) < 180:
        return (delta, ComplimentaryRot(delta))
    else:
        return (ComplimentaryRot(delta), delta)

def InRange(a, r):
    return (a >= r[0]) and (a <= r[1])

def computeShortestPath(prevAngles, currentAngles, frameDuration):
    jointAngleLimits = [(math.degrees(x), math.degrees(y)) for x, y in motion_control.solver.g_jointAngleLimits]
    jointMaxSpeeds = [math.degrees(x) for x in motion_control.solver.g_jointMaxSpeeds]

    finalAngles = []
    for jointIdx in range(6):
        prev = prevAngles[jointIdx]
        shortDelta, longDelta = AngleDelta(prev, currentAngles[jointIdx])
        # Try shortest path first.
        if InRange(prev + shortDelta, jointAngleLimits[jointIdx]):
            delta = shortDelta
        #elif InRange(prev + longDelta, jointAngleLimits[jointIdx]):
        #    # If the shortest path takes us outside the joint limits, try the longest too, just in case.
        #    delta = longDelta
        else:
            print('Joint', jointIdx, 'reaches a rotation of', prev + shortDelta, 'degrees which is outside of the limits', jointAngleLimits[jointIdx], '.')
            return None

        if frameDuration is not None:
            speed = delta / frameDuration
            if abs(speed) > jointMaxSpeeds[jointIdx]:
                print('Joint', jointIdx, 'exceeds maximum speed of', jointMaxSpeeds[jointIdx], 'degrees/s (actual speed:', speed, ').')
                return None

        finalAngles.append(prev + delta)

    return finalAngles
    