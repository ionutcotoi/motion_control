#!/usr/bin/python
# -*- coding: utf-8 -*-

import os
import time
import platform
import ctypes
import threading
import math
import bpy
import mathutils
import motion_control.solver
import motion_control.ui
import socket
import time
import serial

g_moveUsesAngles = True

g_nativeLib = None
g_refreshFunc = None
g_countDevFunc = None
g_devNameFunc = None
g_useDevFunc = None
g_releaseDevFunc = None
g_pollFunc = None
g_shutdownFunc = None

NUM_AXES = 10
NUM_BUTTONS = 20
NUM_DPAD_BUTTONS = 4
LISTEN_HOST = '0.0.0.0'
LISTEN_PORT = 50055
BUTTON_START_REPEAT_DELAY = 0.5
BUTTON_REPEAT_INTERVAL = 0.1

MOVEMENT_STATE_DISCONNECTED = 0
MOVEMENT_STATE_MOVING = 1
MOVEMENT_STATE_NOT_MOVING = 2

class JoyState(ctypes.Structure):
    _fields_ = [
        ('axes', ctypes.c_int * NUM_AXES),
        ('buttons', ctypes.c_int * NUM_BUTTONS),
        ('dpad', ctypes.c_int * NUM_DPAD_BUTTONS)
    ]

class ServerThread(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((LISTEN_HOST, LISTEN_PORT))
        self.sock.listen(1)
        self.clientSocket = None

    def run(self):
        while True:
            try:
                self.clientSocket, addr = self.sock.accept()
            except socket.error:
                return
            print('Accepted connection from', addr)

    def stop(self):
        try:
            self.sock.close()
        except socket.error:
            pass

        if self.clientSocket:
            try:
                self.clientSocket.shutdown(socket.SHUT_RDWR)
                self.clientSocket.close()
            except socket.error:
                pass
            self.clientSocket = None

        self.join()

class JoystickControl:
    POLL_INTERVAL           = 1.0 / 25.0

    AXES_DEADZONE           = 250
    AXES_MAXVAL             = 1000

    MAX_LINEAR_SPEED        = 0.3 * POLL_INTERVAL
    MAX_ANGULAR_SPEED       = math.pi * 0.2 * POLL_INTERVAL
    MAX_EXT_CHANNEL_SPEED   = MAX_LINEAR_SPEED * 5000

    def __init__(self):
        if g_nativeLib == None:
            raise IOError('Joystick driver did not initialize correctly')

        if not g_refreshFunc():
            raise IOError('Cannot obtain list of joysticks')

        numDev = g_countDevFunc()
        if numDev < 1:
            raise IOError('No joysticks found')

        # FIXME: store last joystick name and try to reuse it. Also, add preference for selecting the desired joystick.
        self.joystick = g_useDevFunc(0)
        if self.joystick == None:
            raise IOError('Cannot initialize joystick')

        self.lastState = None
        self.errorPrinted = False

        self.serverThread = ServerThread()
        self.serverThread.start()

        self.SPEED_UP_DPAD_BUTTON       = 0
        self.SPEED_DOWN_DPAD_BUTTON     = 2

        self.PREV_KEY_DPAD_BUTTON       = 3
        self.NEXT_KEY_DPAD_BUTTON       = 1

        osName = platform.system()
        if osName == 'Windows':
            self.CYCLE_MOVE_MODE_BUTTON     = 0
            self.CYCLE_EXT_CHAN_BUTTON      = 1
            self.GOTO_POSE_REV_BUTTON       = 2
            self.GOTO_POSE_BUTTON           = 3
            self.ALT_BUTTON                 = 4
            self.MOVE_TOGETHER_BUTTON       = 5
            self.SET_KEY_BUTTON             = 6
            self.DISABLE_BUTTON             = 7
        else:
            self.CYCLE_MOVE_MODE_BUTTON     = 11
            self.CYCLE_EXT_CHAN_BUTTON      = 12
            self.GOTO_POSE_REV_BUTTON       = 13
            self.GOTO_POSE_BUTTON           = 14
            self.ALT_BUTTON                 = 8
            self.MOVE_TOGETHER_BUTTON       = 9
            self.SET_KEY_BUTTON             = 5
            self.DISABLE_BUTTON             = 4

        self.disabled = False
        self.updateSelectedLocator(None, bpy.data.objects['Robot'])
        self.speedMult = 50
        self.speedSteps = [ 1, 3, 8, 15, 25, 50 ]
        self.selectedExtChan = 0
        self.prevAngles = None
        self.lastCmdTS = None
        self.intfSerialPort = None
        self.dpadNextRepeatTime = [ 0 ] * NUM_DPAD_BUTTONS
        self.buttonNextRepeatTime = [ 0 ] * NUM_BUTTONS
        self.movementState = MOVEMENT_STATE_DISCONNECTED
        self.currentRobotAngles = None
        self.currentExtChannels = None
        self.nextFrameAdvanceTime = 0

        # Try to open the serial port for communication with the interface box
        try:
            # TODO ICOTOI add serial port config panel in blender UI
            if os.name == "nt":
                self.intfSerialPort = serial.Serial(port="COM4", baudrate=115200)
            else:
                self.intfSerialPort = serial.Serial(port="/dev/tty.usbserial-A700eDTT", baudrate=115200)
        except Exception as e:
            print("ERR Serial Port:", e)

        if self.intfSerialPort and self.intfSerialPort.isOpen():
            print("Serial port connected")
        else:
            self.intfSerialPort = None
            print("Cannot open serial port")

    def buttonPressed(self, state, num):
        if (self.lastState == None) or (num < 0) or (num >= NUM_BUTTONS):
            return False

        return self.lastState.buttons[num] and (not state.buttons[num])

    def buttonDownThisFrame(self, state, num):
        if not state.buttons[num]:
            return False

        if not self.lastState:
            return True

        return not self.lastState.buttons[num]

    def buttonPressedOrRepeat(self, state, num):
        now = time.time()

        if not state.buttons[num]:
            return False

        if not self.lastState.buttons[num]:
            # Just pressed.
            self.buttonNextRepeatTime[num] = now + BUTTON_START_REPEAT_DELAY
            return True

        if now < self.buttonNextRepeatTime[num]:
            return False

        self.buttonNextRepeatTime[num] = now + BUTTON_REPEAT_INTERVAL
        return True

    def dpadDirDown(self, state, num):
        if state is None:
            return False

        if platform.system() == 'Windows':
            return state.dpad[num]

        osxMap = [0, 3, 1, 2]
        return state.buttons[osxMap[num]]

    def dpadDirPressed(self, state, num):
        return self.dpadDirDown(state, num) and (not self.dpadDirDown(self.lastState, num))

    def dpadDirPressedOrRepeat(self, state, num):
        now = time.time()

        if not self.dpadDirDown(state, num):
            return False

        if not self.dpadDirDown(self.lastState, num):
            # Just pressed.
            self.dpadNextRepeatTime[num] = now + BUTTON_START_REPEAT_DELAY
            return True

        if now < self.dpadNextRepeatTime[num]:
            return False

        self.dpadNextRepeatTime[num] = BUTTON_REPEAT_INTERVAL
        return True

    def axisValue(self, state, num):
        if (num < 0) or (num >= NUM_AXES):
            return 0

        val = state.axes[num]

        if val < 0:
            val = val + self.AXES_DEADZONE
            if val > 0:
                val = 0
        else:
            val = val - self.AXES_DEADZONE
            if val < 0:
                val = 0

        return 1.0 * val / (self.AXES_MAXVAL - self.AXES_DEADZONE)

    def cycleSelection(self):
        robot = bpy.data.objects['Robot']
        if robot.rig_properties.joy_selection == 'position':
            robot.rig_properties.joy_selection = 'target'
        else:
            robot.rig_properties.joy_selection = 'position'

    def cycleSpace(self):
        robot = bpy.data.objects['Robot']
        if robot.rig_properties.joy_mode == 'world':
            robot.rig_properties.joy_mode = 'camera'
        else:
            robot.rig_properties.joy_mode = 'world'

    def moveTogetherButtonHeld(self, state):
        return (state is not None) and state.buttons[self.MOVE_TOGETHER_BUTTON]

    def updateSelectedLocator(self, state, robot):
        if self.moveTogetherButtonHeld(state):
            self.selectedLocator = 'position + target'
        else:
            self.selectedLocator = robot.rig_properties.joy_selection

    def sendCommand(self, cmd):
        if self.serverThread.clientSocket is None:
            self.movementState = MOVEMENT_STATE_DISCONNECTED
            return False

        try:
            self.serverThread.clientSocket.send(bytes(cmd, 'ascii'))
            self.lastCmdTS = time.time()
            return True
        except socket.error:
            self.serverThread.clientSocket.close()
            self.serverThread.clientSocket = None
            self.movementState = MOVEMENT_STATE_DISCONNECTED
            return False

    def readRobotState(self):
        cmd = 'SYNC\r'
        if not self.sendCommand(cmd):
            return None

        buf = bytes()
        try:
            while True:
                b = self.serverThread.clientSocket.recv(256)
                if len(b) < 1:
                    break
                buf = buf + b
                if b.endswith(b'\r') or b.endswith(b'\n'):
                    break
        except socket.error:
            print('Cannot read sync values from robot')
            self.serverThread.clientSocket.close()
            self.serverThread.clientSocket = None
            return None

        valuesStr = buf.decode('ascii').strip()
        values = valuesStr.split()
        if len(values) < 6:
            return None

        try:
            angles = [ float(x) for x in values[:6] ]
        except ValueError:
            print('Cannot decode angles from robot')
            return None

        return angles

    def syncRobotPosition(self):
        crRobotAngles = self.readRobotState()
        if not crRobotAngles:
            self.movementState = MOVEMENT_STATE_DISCONNECTED
            return

        #print('Got angles:  %.3f %.3f %.3f %.3f %.3f %.3f' % tuple(crRobotAngles))

        motion_control.solver.applyAngles(crRobotAngles)
        bpy.context.scene.update()
        self.prevAngles = crRobotAngles
        self.movementState = MOVEMENT_STATE_MOVING

    def moveSelection(self, state, robot):
        if (self.serverThread.clientSocket is not None) and (self.movementState == MOVEMENT_STATE_DISCONNECTED):
            self.syncRobotPosition()

        linSpeed = self.speedMult * 0.01 * self.MAX_LINEAR_SPEED
        angSpeed = self.speedMult * 0.01 * self.MAX_ANGULAR_SPEED

        v = [ self.axisValue(state, i) * linSpeed for i in [0, 1, 3] ]
        posDelta = mathutils.Vector((-v[1], -v[0], -v[2]))

        rollDelta = self.axisValue(state, 2) * angSpeed
        if (posDelta.length_squared == 0) and (math.fabs(rollDelta) == 0):
            if self.serverThread.clientSocket is not None:
                self.movementState = MOVEMENT_STATE_NOT_MOVING
            return False

        if self.movementState == MOVEMENT_STATE_NOT_MOVING:
            self.syncRobotPosition()
            if self.movementState != MOVEMENT_STATE_MOVING:
                return False

        if robot.rig_properties.joy_mode == 'camera':
            rightDir, forwardDir, upDir = motion_control.solver.getCameraSpace()
            mat = mathutils.Matrix()
            mat[0][0], mat[1][0], mat[2][0] = forwardDir.x, forwardDir.y, forwardDir.z
            mat[0][1], mat[1][1], mat[2][1] = -rightDir.x, -rightDir.y, -rightDir.z
            mat[0][2], mat[1][2], mat[2][2] = -upDir.x, -upDir.y, -upDir.z
            posDelta = mat * posDelta

        if self.moveTogetherButtonHeld(state):
            moveObjs = [ 'Camera_Position', 'Camera_Target' ]
        else:
            if robot.rig_properties.joy_selection == 'position':
                moveObjs = [ 'Camera_Position' ]
            else:
                moveObjs = [ 'Camera_Target' ]

        prevPos = []

        for objName in moveObjs:
            obj = bpy.data.objects[objName]
            prevPos.append(mathutils.Vector(obj.location))
            obj.location = obj.location + posDelta

        prevRoll = robot.rig_properties.cam_roll
        camRoll = prevRoll + rollDelta
        robot.rig_properties.cam_roll = camRoll

        bpy.context.scene.update()

        # Get the camera axes after the movement was performed and update the pole vector. There's a fair amount of
        # redundant computation here, but the code has evolved this way and it's not like we're running into performance
        # problems, so it's better to leave things like this instead of messing with it and breaking some corner cases.
        camPos, camTarget, prevPolePos, roll = motion_control.solver.getRigControls()
        rightDir, forwardDir, upDir = motion_control.solver.computeCamAxes(camPos, camTarget, prevPolePos, roll)
        forwardRotMat = mathutils.Matrix.Rotation(-roll, 3, forwardDir)
        poleDir = forwardRotMat * (-upDir)
        polePos = poleDir * prevPolePos.length
        poleLocator = bpy.data.objects['Camera_Pole']
        poleLocator.location = polePos

        bpy.context.scene.update()

        solution = motion_control.solver.solveRobot(camPos.x, camPos.y, camPos.z, camTarget.x, camTarget.y, camTarget.z, camRoll, polePos)
        reachable = solution[6]
        solution = [math.degrees(x) for x in solution[:6]]

        if reachable and g_moveUsesAngles and self.prevAngles:
            solution = motion_control.solver.computeShortestPath(self.prevAngles, solution, None)
            if not solution:
                reachable = False

        if not reachable:
            # Unreachable, undo move.
            for idx in range(len(moveObjs)):
                obj = bpy.data.objects[moveObjs[idx]]
                obj.location = prevPos[idx]
            robot.rig_properties.cam_roll = prevRoll
            poleLocator.location = prevPolePos
            bpy.context.scene.update()
            return True

        if self.serverThread.clientSocket and self.prevAngles:
            cmd = self.buildMoveCmd(solution)
            self.sendCommand(cmd)
            self.prevAngles = solution

        return True

    def cycleSelExtChan(self, robot, inc):
        startChan = self.selectedExtChan
        while True:
            self.selectedExtChan = (self.selectedExtChan + inc) % motion_control.ui.RigProperties.NUM_EXTERNAL_CHANS
            if self.selectedExtChan == startChan:
                # We've reached the channel where we started, give up.
                return False

            if getattr(robot.rig_properties, 'extern_chan_%d_ena' % self.selectedExtChan):
                # Found channel that's enabled, use it.
                return True

    def readExtChannels(self):
        robot = bpy.data.objects['Robot']

        if not self.intfSerialPort:
            return None

        allExtVals = [ getattr(robot.rig_properties, 'extern_chan_%d_val' % i) for i in range(motion_control.ui.RigProperties.NUM_EXTERNAL_CHANS) ]
        # FIXME: get this from the serial link.
        return allExtVals

    def sendExtChannels(self, chan, val):
        if not self.intfSerialPort:
            return

        # FIXME: send the values.
        focus = int(val)
        moveCmd = "MOVE " + str(chan) + " " + str(focus) + "\n"
        print(moveCmd)
        self.intfSerialPort.write(moveCmd.encode('ascii'))

        # Empty the serial buffer
        while self.intfSerialPort.inWaiting() > 0:
            self.intfSerialPort.read(1)

    def moveSelectedChan(self, state, robot):
        if not getattr(robot.rig_properties, 'extern_chan_%d_ena' % self.selectedExtChan):
            return False

        extChanName = getattr(robot.rig_properties, 'extern_chan_%d_name' % self.selectedExtChan)

        if self.intfSerialPort:
            extChannels = self.readExtChannels()
            if not extChannels:
                return False

            for i in range(len(extChannels)):
                setattr(robot.rig_properties, 'extern_chan_%d_val' % i, extChannels[i])

        linSpeed = self.speedMult * 0.01 * self.MAX_EXT_CHANNEL_SPEED
        deltas = [ self.axisValue(state, i) * linSpeed for i in [4, 5] ]
        delta = deltas[1] - deltas[0]
        if abs(delta) < 0.01:
            return False

        val = getattr(robot.rig_properties, 'extern_chan_%d_val' % self.selectedExtChan)
        val = val + delta
        val = min(10000, max(0, val))
        setattr(robot.rig_properties, 'extern_chan_%d_val' % self.selectedExtChan, val)

        allExtVals = [ getattr(robot.rig_properties, 'extern_chan_%d_val' % i) for i in range(motion_control.ui.RigProperties.NUM_EXTERNAL_CHANS) ]
        self.sendExtChannels(extChanName, val)
        return True

    def editKeyframe(self, obj, dataPath, remove):
        if not remove:
            obj.keyframe_insert(data_path=dataPath)
        else:
            obj.keyframe_delete(data_path=dataPath)

    def snapToKeyframe(self, f, inc):
        keyframes = sorted(motion_control.ui.GatherKeyframes())
        if len(keyframes) == 0:
            return f

        f = f + inc
        nextIdx = None
        for i in range(len(keyframes)):
            if keyframes[i] == f:
                return f

            if keyframes[i] > f:
                nextIdx = i
                break

        if nextIdx == None:
            # No next keyframe. Return original keyframe if going forward or
            # last keyframe if going backwards.
            return f - inc if inc > 0 else keyframes[-1]

        if inc < 0:
            if nextIdx < 1:
                # No previous keyframe, return original frame if going backwards.
                return f - inc
            else:
                newIdx = nextIdx - 1
        else:
            newIdx = nextIdx

        return keyframes[newIdx]

    def gotoFrame(self, inc, onlyKeys):
        f = bpy.context.scene.frame_current

        if onlyKeys:
            f = self.snapToKeyframe(f, inc)
        else:
            f = f + inc
            f = max(0, f)

        bpy.context.scene.frame_set(f)

    def changeSpeed(self, inc, continuous):
        self.speedMult = max(1, min(50, self.speedMult + inc))

        if continuous:
            return

        # Find next step value.
        nextIdx = None
        for i in range(len(self.speedSteps)):
            if self.speedSteps[i] == self.speedMult:
                # We're exactly at a step.
                return

            if self.speedSteps[i] > self.speedMult:
                nextIdx = i
                break

        assert( (nextIdx != None) and (nextIdx > 0) )

        # Use next step when incrementing, previous step when decrementing.
        newIdx = nextIdx if inc > 0 else nextIdx - 1
        self.speedMult = self.speedSteps[newIdx]

    def getExtChanValue(self, robot, idx):
        if getattr(robot.rig_properties, 'extern_chan_%d_ena' % idx):
            return getattr(robot.rig_properties, 'extern_chan_%d_val' % idx)
        else:
            return -1

    def followVectorGoal(self, crVect, targetVect, speed):
        delta = targetVect - crVect
        dist = delta.length

        if dist <= speed:
            # We can reach the target in one step at the current speed.
            return (targetVect, False)

        newVect = crVect + delta.normalized() * speed
        return (newVect, True)

    def followScalarGoal(self, crVal, targetVal, speed):
        delta = targetVal - crVal
        absDelta = abs(delta)

        if absDelta <= speed:
            return (targetVal, False)

        step = speed if delta > 0 else -speed
        newVal = crVal + step
        return (newVal, True)

    def buildMoveCmd(self, jointAngles):
        if g_moveUsesAngles:
            cmd = 'MOVE %.3f %.3f %.3f %.3f %.3f %.3f\r' % tuple(jointAngles)
            #print('Sent angles:', cmd[5:])
            return cmd

        flangePos, flangeRot, flangeCfg = motion_control.solver.getEffectorPoint(jointAngles)

        cmd = 'MOVP '
        cmd = cmd + ' '.join(flangePos) + ' '
        cmd = cmd + ' '.join(flangeRot) + ' '
        cmd = cmd + ' '.join(flangeCfg) + '\r'

        return cmd

    def gotoBlenderPose(self, justEntered, advanceFrameDir):
        now = time.time()

        if justEntered:
            self.nextFrameAdvanceTime = now + BUTTON_REPEAT_INTERVAL

        if justEntered or (not self.currentRobotAngles):
            self.currentRobotAngles = self.readRobotState()

        if justEntered or (not self.currentExtChannels):
            self.currentExtChannels = self.readExtChannels()

        robot = bpy.data.objects['Robot']
        stateChanged = False

        if self.currentRobotAngles:
            # We're connected to the robot, move it.
            crCamPos, crLookatPos, crPolePos, crRoll = motion_control.solver.controlsFromAngles(self.currentRobotAngles)
            targetCamPos, targetLookatPos, targetPolePos, targetRoll = motion_control.solver.getRigControls()

            linSpeed = self.speedMult * 0.01 * self.MAX_LINEAR_SPEED
            angSpeed = self.speedMult * 0.01 * self.MAX_ANGULAR_SPEED
            extChanSpeed = self.speedMult * 0.01 * self.MAX_EXT_CHANNEL_SPEED

            crCamPos, posChanged = self.followVectorGoal(crCamPos, targetCamPos, linSpeed)
            crLookatPos, lookatChanged = self.followVectorGoal(crLookatPos, targetLookatPos, linSpeed)
            crPolePos, poleChanged = self.followVectorGoal(crPolePos, targetPolePos, linSpeed)
            crRoll, rollChanged = self.followScalarGoal(crRoll, targetRoll, angSpeed)

            solution = motion_control.solver.solveRobot(crCamPos.x, crCamPos.y, crCamPos.z, crLookatPos.x, crLookatPos.y, crLookatPos.z, crRoll, crPolePos)
            if not solution[6]:
                # The Blender pose represents and unreachable configuration, don't do anything.
                return

            solution = [math.degrees(x) for x in solution[:6]]

            if g_moveUsesAngles:
                solution = motion_control.solver.computeShortestPath(self.currentRobotAngles, solution, None)
                if not solution:
                    return

            cmd = self.buildMoveCmd(solution)
            self.sendCommand(cmd)

            self.currentRobotAngles = solution
            stateChanged = posChanged or lookatChanged or poleChanged or rollChanged

        if self.currentExtChannels:
            # We're connected to the external channels, update them.
            targetChannels = [ self.getExtChanValue(robot, i) for i in range(motion_control.ui.RigProperties.NUM_EXTERNAL_CHANS) ]
            for i in range(len(self.currentExtChannels)):
                if targetChannels[i] != -1:
                    valChanged = (self.currentExtChannels[i] - targetChannels[i])!=0
                    self.currentExtChannels[i] = targetChannels[i]
                    #self.currentExtChannels[i], valChanged = self.followScalarGoal(self.currentExtChannels[i], targetChannels[i], extChanSpeed)
                    stateChanged = stateChanged or valChanged
                    # if stateChanged:
                    extChanName = getattr(robot.rig_properties, 'extern_chan_%d_name' % i)
                    extChanValue = self.getExtChanValue(robot, i)
                    self.sendExtChannels(extChanName, extChanValue)
                else:
                    self.currentExtChannels[i] = -1



        if (not stateChanged) and (advanceFrameDir != 0) and (now >= self.nextFrameAdvanceTime):
            currentFrame = bpy.context.scene.frame_current
            newFrame = max(currentFrame + advanceFrameDir, 0)
            bpy.context.scene.frame_set(newFrame)
            self.nextFrameAdvanceTime = now + BUTTON_REPEAT_INTERVAL

    def pollJoystick(self):
        state = JoyState()

        if not g_pollFunc(self.joystick, ctypes.pointer(state)):
            if not self.errorPrinted:
                print('Joystick poll failed.')
                self.errorPrinted = True
            return None

        self.errorPrinted = False

        #s = [ '1' if x else '0' for x in state.buttons ]
        #print(s)

        robot = bpy.data.objects['Robot']
        needsRedraw = False

        if self.serverThread.clientSocket and self.lastCmdTS and (time.time() - self.lastCmdTS > 5):
            self.sendCommand('PING\r')

        if self.buttonPressed(state, self.DISABLE_BUTTON):
            if not self.disabled:
                self.disabled = True
                motion_control.ui.ForceRedraw()
                return state

            self.disabled = False
            needsRedraw = True

        if self.disabled:
            return state

        altHeld = state.buttons[self.ALT_BUTTON]

        if self.dpadDirPressedOrRepeat(state, self.SPEED_UP_DPAD_BUTTON) and (self.speedMult < 50):
            self.changeSpeed(1, altHeld)
            needsRedraw = True

        if self.dpadDirPressedOrRepeat(state, self.SPEED_DOWN_DPAD_BUTTON) and (self.speedMult > 1):
            self.changeSpeed(-1, altHeld)
            needsRedraw = True

        if state.buttons[self.GOTO_POSE_BUTTON] and self.serverThread.clientSocket:
            justEntered = self.buttonDownThisFrame(state, self.GOTO_POSE_BUTTON)
            advDir = 1 if altHeld else 0
            self.gotoBlenderPose(justEntered, advDir)
            return state

        if altHeld and state.buttons[self.GOTO_POSE_REV_BUTTON] and self.serverThread.clientSocket:
            justEntered = self.buttonDownThisFrame(state, self.GOTO_POSE_REV_BUTTON)
            self.gotoBlenderPose(justEntered, -1)
            return state

        if self.buttonPressed(state, self.CYCLE_MOVE_MODE_BUTTON):
            if altHeld:
                self.cycleSpace()
            else:
                self.cycleSelection()
            needsRedraw = True

        if self.dpadDirPressedOrRepeat(state, self.PREV_KEY_DPAD_BUTTON):
            self.gotoFrame(-1, altHeld)

        if self.dpadDirPressedOrRepeat(state, self.NEXT_KEY_DPAD_BUTTON):
            self.gotoFrame(1, altHeld)

        if self.buttonPressed(state, self.CYCLE_EXT_CHAN_BUTTON):
            inc = -1 if altHeld else 1
            needsRedraw = self.cycleSelExtChan(robot, inc)

        if self.buttonPressed(state, self.SET_KEY_BUTTON):
            # Key the locators.
            for objName in [ 'Camera_Position', 'Camera_Target', 'Camera_Pole' ]:
                obj = bpy.data.objects[objName]
                self.editKeyframe(obj, 'location', altHeld)

            # Key the camera roll.
            self.editKeyframe(robot, 'rig_properties.cam_roll', altHeld)

            # Key the external channels.
            for i in range(motion_control.ui.RigProperties.NUM_EXTERNAL_CHANS):
                if getattr(robot.rig_properties, 'extern_chan_%d_ena' % i):
                    self.editKeyframe(robot, ('rig_properties.extern_chan_%d_val' % i), altHeld)

        self.updateSelectedLocator(state, robot)
        if self.moveTogetherButtonHeld(state) != self.moveTogetherButtonHeld(self.lastState):
            needsRedraw = True

        if self.moveSelectedChan(state, robot):
            needsRedraw = True

        if self.moveSelection(state, robot):
            # Moving the controls refreshes the viewport, so no need to refresh it explicitly.
            needsRedraw = False

        if needsRedraw:
            motion_control.ui.ForceRedraw()

        return state

    def run(self):
        self.lastState = self.pollJoystick()

    def stop(self):
        g_releaseDevFunc(self.joystick)
        self.joystick = None

        if self.serverThread is not None:
            self.serverThread.stop()
            self.serverThread = None

        if self.intfSerialPort is not None:
            self.intfSerialPort = None

def init():
    global g_nativeLib
    global g_refreshFunc
    global g_countDevFunc
    global g_devNameFunc
    global g_useDevFunc
    global g_releaseDevFunc
    global g_pollFunc
    global g_shutdownFunc

    modPath = os.path.abspath(os.path.dirname(__file__))
    osName = platform.system()
    osBits = platform.architecture()[0]

    try:
        if osName == 'Windows':
            if osBits == '64bit':
                dllName = 'joystick_win64.dll'
            else:
                dllName = 'joystick_win32.dll'
            libPath = os.path.join(modPath, dllName)
            g_nativeLib = ctypes.WinDLL(libPath)
        else:
            if osName == 'Darwin':
                soName = 'joystick_osx.so'
            else:
                soName = 'joystick_linux.so'
            libPath = os.path.join(modPath, soName)
            g_nativeLib = ctypes.CDLL(libPath, mode=ctypes.RTLD_LOCAL)
    except OSError:
        print('Could not load joystick library from "' + libPath + '".')
        return False

    if osName == 'Windows':
        initProto = ctypes.WINFUNCTYPE(ctypes.c_int)
        shutdownProto = ctypes.WINFUNCTYPE(None)
        refreshProto = ctypes.WINFUNCTYPE(ctypes.c_int)
        countProto = ctypes.WINFUNCTYPE(ctypes.c_int)
        devNameProto = ctypes.WINFUNCTYPE(ctypes.c_char_p, ctypes.c_int)
        useDevProto = ctypes.WINFUNCTYPE(ctypes.c_void_p, ctypes.c_int)
        releaseDevProto = ctypes.WINFUNCTYPE(None, ctypes.c_void_p)
        pollProto = ctypes.WINFUNCTYPE(ctypes.c_int, ctypes.c_void_p, ctypes.POINTER(JoyState))
    else:
        initProto = ctypes.CFUNCTYPE(ctypes.c_int)
        shutdownProto = ctypes.CFUNCTYPE(None)
        refreshProto = ctypes.CFUNCTYPE(ctypes.c_int)
        countProto = ctypes.CFUNCTYPE(ctypes.c_int)
        devNameProto = ctypes.CFUNCTYPE(ctypes.c_char_p, ctypes.c_int)
        useDevProto = ctypes.CFUNCTYPE(ctypes.c_void_p, ctypes.c_int)
        releaseDevProto = ctypes.CFUNCTYPE(None, ctypes.c_void_p)
        pollProto = ctypes.CFUNCTYPE(ctypes.c_int, ctypes.c_void_p, ctypes.POINTER(JoyState))

    try:
        initFunc = initProto(('JoyInit', g_nativeLib))
        g_shutdownFunc = shutdownProto(('JoyShutdown', g_nativeLib))
        g_refreshFunc = refreshProto(('JoyRefreshDevices', g_nativeLib))
        g_countDevFunc = countProto(('JoyCountDevices', g_nativeLib))
        g_devNameFunc = devNameProto(('JoyGetDeviceName', g_nativeLib))
        g_useDevFunc = useDevProto(('JoyUseDevice', g_nativeLib))
        g_releaseDevFunc = releaseDevProto(('JoyReleaseDevice', g_nativeLib))
        g_pollFunc = pollProto(('JoyPollDevice', g_nativeLib))
    except AttributeError:
        print('Cannot find the required functions in "' + libPath + '"')
        return False

    if not initFunc():
        print('Joystick init failed.')
        shutdown()
        return False

    return True

def shutdown():
    global g_nativeLib

    if g_shutdownFunc is not None:
        g_shutdownFunc()

    if g_nativeLib is not None:
        del g_nativeLib
        g_nativeLib = None
