#ifndef JOYSTICKPCH_H
#define JOYSTICKPCH_H

#if defined(_WIN32)
#	define WIN32_LEAN_AND_MEAN
#	define NOMINMAX
#	define _WIN32_WINNT					0x0500
#	define WINVER						0x0500
#	define _CRT_SECURE_NO_DEPRECATE		1
#	define _CRT_NONSTDC_NO_DEPRECATE	1
#	define STRICT
#	define DIRECTINPUT_VERSION			0x0800
#	include <windows.h>
#	include <dinput.h>
#	include <dinputd.h>
#	define DLLEXPORT					__declspec(dllexport)
#elif defined(__APPLE__)
#	include <Kernel/IOKit/hidsystem/IOHIDUsageTables.h>
#	include <IOKit/IOKitLib.h>
#	include <IOKit/IOCFPlugIn.h>
#	include <IOKit/hid/IOHIDLib.h>
#	include <IOKit/hid/IOHIDKeys.h>
#	include <IOKit/IOMessage.h>
#	include <CoreFoundation/CoreFoundation.h>
#	include <Carbon/Carbon.h>
#	define DLLEXPORT					__attribute__((visibility("default")))
#else
#	error Unknown platform.
#endif

#include <vector>

#endif
