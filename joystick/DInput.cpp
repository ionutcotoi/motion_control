#include "JoystickPCH.h"
#include "APIWrapper.h"

struct JoyInfo
{
	JoyInfo()
	{
		dev = NULL;
		name[0] = 0;
	}

	JoyInfo(const JoyInfo& other)
	{
		dev = other.dev;
		dev->AddRef();
		memcpy(name, other.name, sizeof(name));
	}

	~JoyInfo()
	{
		if(dev)
		{
			dev->Unacquire();
			dev->Release();
		}
	}

	IDirectInputDevice8*	dev;
	char					name[256];
};

static IDirectInput8*				g_dinput = NULL;
static std::vector<JoyInfo>			g_joysticks;

static BOOL CALLBACK EnumJoysticksCB(const DIDEVICEINSTANCE* devInstance, void* /*context*/)
{
	IDirectInputDevice8* joystick = NULL;
	HRESULT hr = g_dinput->CreateDevice(devInstance->guidInstance, &joystick, NULL);
	if(FAILED(hr))
		return DIENUM_CONTINUE;

	DIDEVICEINSTANCE instInfo;
	instInfo.dwSize = sizeof(instInfo);
	hr = joystick->GetDeviceInfo(&instInfo);
	if(FAILED(hr))
		return DIENUM_CONTINUE;

	JoyInfo info;
	info.dev = joystick;

	// Poor man's UTF-8 conversion (pretend only ASCII exists).
	size_t i = 0; 
	while((i < sizeof(info.name) - 1) && (instInfo.tszInstanceName[i] != 0))
	{
		info.name[i] = (char)(instInfo.tszInstanceName[i] & 0xff);
		++i;
	}
	info.name[i] = 0;

	g_joysticks.push_back(info);
	return DIENUM_CONTINUE;
}

int JoyInit()
{
	HMODULE mod = GetModuleHandle(NULL);
	HRESULT hr = DirectInput8Create(mod, DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&g_dinput, NULL);
	if(FAILED(hr))
		return 0;

	return JoyRefreshDevices();
}

void JoyShutdown()
{
	g_joysticks.clear();
	if(g_dinput)
		g_dinput->Release();
}

int JoyRefreshDevices()
{
	g_joysticks.resize(0);
	g_joysticks.reserve(10);

	HRESULT hr = g_dinput->EnumDevices(DI8DEVCLASS_GAMECTRL, EnumJoysticksCB, NULL, DIEDFL_ATTACHEDONLY);
	return SUCCEEDED(hr);
}

int JoyCountDevices()
{
	return (int)g_joysticks.size();
}

const char* JoyGetDeviceName(int index)
{
	if( (index < 0) || (index >= (int)g_joysticks.size()) )
		return "";

	return g_joysticks[index].name;
}

static BOOL CALLBACK EnumObjectsCB(const DIDEVICEOBJECTINSTANCE* devObj, void* context)
{
	IDirectInputDevice8* dev = (IDirectInputDevice8*)context;

	if(devObj->dwType & DIDFT_AXIS)
	{
		// Set the range for the axis.
		DIPROPRANGE diprg;
		diprg.diph.dwSize = sizeof(diprg);
		diprg.diph.dwHeaderSize = sizeof(DIPROPHEADER);
		diprg.diph.dwHow = DIPH_BYID;
		diprg.diph.dwObj = devObj->dwType;
		diprg.lMin = -1000;
		diprg.lMax = +1000;

		dev->SetProperty(DIPROP_RANGE, &diprg.diph);
	}

	return DIENUM_CONTINUE;
}

static BOOL CALLBACK EnumWndCB(HWND hwnd, LPARAM lParam)
{
	DWORD wndProcID;
	GetWindowThreadProcessId(hwnd, &wndProcID);

	if(wndProcID == GetCurrentProcessId())
	{
		HWND* out = (HWND*)lParam;
		*out = hwnd;
		return FALSE;
	}

	return TRUE;
}

static HWND FindTopmostWindow()
{
	// Find a window which belongs to the current process.
	HWND hwnd = NULL;
	EnumWindows((WNDENUMPROC)EnumWndCB, (LPARAM)&hwnd);
	if(!hwnd)
		return NULL;

	// Go up the hierarchy until we find a window without a parent.
	DWORD thisProc = GetCurrentProcessId();
	for(;;)
	{
		HWND parent = GetParent(hwnd);
		if(!parent)
			return hwnd;

		DWORD wndProcID;
		GetWindowThreadProcessId(hwnd, &wndProcID);
		if(wndProcID != thisProc)
			return hwnd;

		hwnd = parent;
	}
}

void* JoyUseDevice(int index)
{
	if( (index < 0) || (index >= (int)g_joysticks.size()) )
		return NULL;

	IDirectInputDevice8* dev = g_joysticks[index].dev;
	HRESULT hr = dev->SetDataFormat(&c_dfDIJoystick2);
	if(FAILED(hr))
		return NULL;

	HWND wnd = FindTopmostWindow();
	hr = dev->SetCooperativeLevel(wnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
	if(FAILED(hr))
		return NULL;

	hr = dev->EnumObjects(EnumObjectsCB, dev, DIDFT_ALL);
	if(FAILED(hr))
		return NULL;

	dev->AddRef();
	return dev;
}

void JoyReleaseDevice(void* joystick)
{
	IDirectInputDevice8* dev = (IDirectInputDevice8*)joystick;
	if(!dev)
		return;

	dev->Unacquire();
	dev->Release();
}

int JoyPollDevice(void* joystick, JoyState& state)
{
	memset(&state, 0, sizeof(state));

	IDirectInputDevice8* dev = (IDirectInputDevice8*)joystick;
	if(!dev)
		return 0;

	HRESULT hr = dev->Poll();
	if(FAILED(hr))
	{
		hr = dev->Acquire();
		while(hr == DIERR_INPUTLOST)
			hr = dev->Acquire();

		if(FAILED(hr))
			return 0;
	}

	DIJOYSTATE2 js;
	hr = dev->GetDeviceState(sizeof(DIJOYSTATE2), &js);
	if(FAILED(hr))
		return 0;

	state.axes[0] = (int)js.lX;
	state.axes[1] = (int)js.lY;
	state.axes[2] = (int)js.lRx;
	state.axes[3] = (int)js.lRy;
	state.axes[4] = (int)js.lZ;
	state.axes[5] = (int)js.lRz;

	size_t maxButtons = sizeof(state.buttons) / sizeof(state.buttons[0]);
	for(size_t i = 0; i < maxButtons; ++i)
		state.buttons[i] = ((js.rgbButtons[i] & 0x80) != 0);

	switch(js.rgdwPOV[0])
	{
		case 0:
			state.dpad[0] = 1;
			break;
		case 4500:
			state.dpad[0] = 1;
			state.dpad[1] = 1;
			break;
		case 9000:
			state.dpad[1] = 1;
			break;
		case 13500:
			state.dpad[1] = 1;
			state.dpad[2] = 1;
			break;
		case 18000:
			state.dpad[2] = 1;
			break;
		case 22500:
			state.dpad[2] = 1;
			state.dpad[3] = 1;
			break;
		case 27000:
			state.dpad[3] = 1;
			break;
		case 31500:
			state.dpad[3] = 1;
			state.dpad[0] = 1;
			break;
	}

	return 1;
}
