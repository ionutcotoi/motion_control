#!/bin/bash

SOPATH=../addons/motion_control/joystick_osx.so
g++ -O2 -DNDEBUG -arch x86_64 -fPIC -fvisibility=hidden -shared -o ${SOPATH} HIDManager.cpp -framework IOKit -framework Carbon
strip -x ${SOPATH}
rm -fr ${SOPATH}.dSYM
