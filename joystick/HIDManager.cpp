#include "JoystickPCH.h"
#include "APIWrapper.h"

struct ElemInfo
{
	ElemInfo()
	{
		cookie = 0;
		min = 0;
		max = 0;
	}

	long 	cookie;
	long 	min;
	long 	max;
};

struct JoyInfo
{
	JoyInfo()
	{
		interface = NULL;
		notificationPort = NULL;
		portIterator = 0;
		name[0] = 0;
		removed = false;

		axes.reserve(8);
		buttons.reserve(20);
	}

	~JoyInfo()
	{
		if(interface)
		{
			(*interface)->close(interface);
			(*interface)->Release(interface);
		}

		if(notificationPort)
			IONotificationPortDestroy(notificationPort);

        if(portIterator)
            IOObjectRelease(portIterator);
	}

	io_object_t					handle;
    IOHIDDeviceInterface**		interface;
    IONotificationPortRef		notificationPort;
    io_iterator_t				portIterator;
    char						name[256];
    bool						removed;

    std::vector<ElemInfo>		axes;
    std::vector<ElemInfo>		buttons;
};

static IONotificationPortRef	g_notificationPort = NULL;
static std::vector<JoyInfo*>	g_devices;

static bool NumberForKey(CFDictionaryRef dict, CFStringRef key, long* val)
{
	if(!dict)
		return false;

    CFNumberRef elem = (CFNumberRef)CFDictionaryGetValue(dict, key);
    return (elem != NULL) && (CFGetTypeID(elem) == CFNumberGetTypeID()) && CFNumberGetValue(elem, kCFNumberLongType, val);
}

static bool StringForKey(CFDictionaryRef dict, CFStringRef key, char* str, int strSize)
{
	if(!dict)
		return false;
	
    CFStringRef elem = (CFStringRef)CFDictionaryGetValue(dict, key);
    return (elem != NULL) && (CFGetTypeID(elem) == CFStringGetTypeID()) && CFStringGetCString(elem, str, strSize, CFStringGetSystemEncoding());
}

static void ReadElementDictionary(JoyInfo* joystick, CFDictionaryRef devProperties);

static void ProcessElement(const void* value, void* parameter)
{
	if(CFGetTypeID(value) != CFDictionaryGetTypeID())
	{
		printf("Element value is not a dictionary.\n");
		return;
	}

	JoyInfo* joystick = (JoyInfo*)parameter;
	CFDictionaryRef elemDict = (CFDictionaryRef)value;

    long type;
    if(!NumberForKey(elemDict, CFSTR(kIOHIDElementTypeKey), &type))
    {
    	printf("No type for element.\n");
    	return;
    }

    if(type == kIOHIDElementTypeCollection)
    {
    	ReadElementDictionary(joystick, elemDict);
    	return;
    }

    if( (type != kIOHIDElementTypeInput_Misc) && (type == kIOHIDElementTypeInput_Button) && (type == kIOHIDElementTypeInput_Axis) )
    {
    	printf("Skipping element of type %ld\n", type);
    	return;
    }

    long usagePage, usage;
    if(!NumberForKey(elemDict, CFSTR(kIOHIDElementUsagePageKey), &usagePage) || !NumberForKey(elemDict, CFSTR(kIOHIDElementUsageKey), &usage))
    {
    	printf("No usage page or usage type for element.\n");
    	return;
    }

    ElemInfo elem;
    if(!NumberForKey(elemDict, CFSTR(kIOHIDElementCookieKey), &elem.cookie))
    {
    	printf("No cookie for element.\n");
    	return;
    }

    NumberForKey(elemDict, CFSTR(kIOHIDElementMinKey), &elem.min);
    NumberForKey(elemDict, CFSTR(kIOHIDElementMaxKey), &elem.max);

    enum ElemFunction
    {
    	ElemFunction_None = 0,
    	ElemFunction_Axis,
    	ElemFunction_Button
    } function = ElemFunction_None;

    switch(usagePage)
    {
        case kHIDPage_GenericDesktop:
        {
            switch(usage)
            {
                case kHIDUsage_GD_X:
                case kHIDUsage_GD_Y:
                case kHIDUsage_GD_Z:
                case kHIDUsage_GD_Rx:
                case kHIDUsage_GD_Ry:
                case kHIDUsage_GD_Rz:
                case kHIDUsage_GD_Slider:
                case kHIDUsage_GD_Dial:
                case kHIDUsage_GD_Wheel:
                	function = ElemFunction_Axis;
                    break;
            }
            break;
        }

        case kHIDPage_Simulation:
        {
            switch(usage)
            {
                case kHIDUsage_Sim_Rudder:
                case kHIDUsage_Sim_Throttle:
                	function = ElemFunction_Axis;
                    break;
            }
            break;
        }

        case kHIDPage_Button:
       	{
       		function = ElemFunction_Button;
	        break;
	    }
    }

    switch(function)
    {
    	case ElemFunction_Axis:
    		joystick->axes.push_back(elem);
    		break;

    	case ElemFunction_Button:
    		joystick->buttons.push_back(elem);
    		break;

    	default:
    		printf("Unknown element usage, skipping.\n");
	    	break;
    }
}

static void ReadElementDictionary(JoyInfo* joystick, CFDictionaryRef devProperties)
{
    CFArrayRef arr = (CFArrayRef)CFDictionaryGetValue(devProperties, CFSTR(kIOHIDElementKey));
    if( (arr == NULL) || (CFGetTypeID(arr) != CFArrayGetTypeID()) )
    {
    	printf("Properties dictionary doesn't have a kIOHIDElementKey key\n");
    	return;
    }

    CFRange range = { 0, CFArrayGetCount(arr) };
    CFArrayApplyFunction(arr, range, ProcessElement, joystick);
}

static bool QueryDevice(JoyInfo* joystick)
{
	CFMutableDictionaryRef hidProperties = NULL;
    kern_return_t propsCreated = IORegistryEntryCreateCFProperties(joystick->handle, &hidProperties, kCFAllocatorDefault, kNilOptions);
    if( (propsCreated != KERN_SUCCESS) || !hidProperties)
    {
    	printf("IORegistryEntryCreateCFProperties() failed.\n");
    	return false;
    }

    // Apparently we get some properties through the HID dictionary and some through the USB dictionary. OSX
    // is too retarded to put everything in one place.

    CFMutableDictionaryRef usbProperties = NULL;
    io_registry_entry_t parent1 = 0, parent2 = 0;

    bool hasUSBInfo = 
    	(IORegistryEntryGetParentEntry(joystick->handle, kIOServicePlane, &parent1) == KERN_SUCCESS) &&
    	(IORegistryEntryGetParentEntry(parent1, kIOServicePlane, &parent2) == KERN_SUCCESS) &&
    	(IORegistryEntryCreateCFProperties(parent2, &usbProperties, kCFAllocatorDefault, kNilOptions) == KERN_SUCCESS) &&
    	(usbProperties != NULL);

    if(!StringForKey(hidProperties, CFSTR(kIOHIDProductKey), joystick->name, sizeof(joystick->name)))
    {
	    if(!StringForKey(usbProperties, CFSTR("USB Product Name"), joystick->name, sizeof(joystick->name)))
	    	strcpy(joystick->name, "UNKNOWN");
    }

    if(parent1)
    	IOObjectRelease(parent1);

    if(parent2)
    	IOObjectRelease(parent2);

    if(usbProperties)
    	CFRelease(usbProperties);

    long usagePage = -1, usage = -1;
    bool use = false;
    if(NumberForKey(hidProperties, CFSTR(kIOHIDPrimaryUsagePageKey), &usagePage) && NumberForKey(hidProperties, CFSTR(kIOHIDPrimaryUsageKey), &usage))
    {
	    if(usagePage == kHIDPage_GenericDesktop)
    	{
    		use = 
    			(usage == kHIDUsage_GD_Joystick) ||
        		(usage == kHIDUsage_GD_GamePad) ||
        		(usage == kHIDUsage_GD_MultiAxisController);
	    }
	}

	if(!use)
    {
    	printf("Cannot use device '%s': usage page %ld, usage %ld\n", joystick->name, usagePage, usage);
		CFRelease(hidProperties);
	    return false;
    }

    ReadElementDictionary(joystick, hidProperties);

	CFRelease(hidProperties);
    return true;
}

static bool OpenInterface(JoyInfo* joystick)
{
    SInt32 score = 0;
    IOCFPlugInInterface** plugInInterface = NULL;
    IOReturn result = IOCreatePlugInInterfaceForService(joystick->handle, kIOHIDDeviceUserClientTypeID, kIOCFPlugInInterfaceID, &plugInInterface, &score);
    if(result != kIOReturnSuccess)
    {
    	printf("IOCreatePlugInInterfaceForService() failed.\n");
    	return false;
    }

    HRESULT pluginResult = (*plugInInterface)->QueryInterface(plugInInterface, CFUUIDGetUUIDBytes(kIOHIDDeviceInterfaceID), (void**)&joystick->interface);
    if(pluginResult != S_OK)
    {
    	printf("QueryInterface() failed.\n");
        (*plugInInterface)->Release(plugInInterface);
        return false;
    }

    result = (*joystick->interface)->open(joystick->interface, 0);
    if(result != kIOReturnSuccess)
    {
    	printf("IOHIDDeviceInterface::open() failed.\n");
        return false;
    }

    return true;
}

static void DeviceRemovedCallback(void* target, IOReturn result, void* refcon, void* sender)
{
    JoyInfo* device = (JoyInfo*)refcon;
    device->removed = true;
    printf("DeviceRemovedCallback(): %s\n", device->name);
}

static void PortRemovedCallback(void* refcon, io_service_t service, natural_t messageType, void* messageArgument)
{
    if( (refcon != NULL) && (messageType == kIOMessageServiceIsTerminated) )
    {
        JoyInfo* device = (JoyInfo*)refcon;
        printf("PortRemovedCallback(): %s\n", device->name);
        device->removed = 1;
    }
}

static void AddDevice(io_object_t devHandle)
{
    JoyInfo* joystick = new JoyInfo;
    joystick->handle = devHandle;

	if(!QueryDevice(joystick))
	{
		delete joystick;
		return;
	}

	if(!OpenInterface(joystick))
    {
    	delete joystick;
    	return;
    }

    g_devices.push_back(joystick);

    // Ask to be notified when it gets removed.
    (*joystick->interface)->setRemovalCallback(joystick->interface, DeviceRemovedCallback, joystick, NULL);
    joystick-> notificationPort = IONotificationPortCreate(kIOMasterPortDefault);
    CFRunLoopAddSource(CFRunLoopGetCurrent(), IONotificationPortGetRunLoopSource(joystick->notificationPort), kCFRunLoopDefaultMode);
	IOServiceAddInterestNotification(joystick->notificationPort, devHandle, kIOGeneralInterest, PortRemovedCallback, joystick, &joystick->portIterator);

	printf("Added device: %s, %d axes, %d buttons.\n", joystick->name, (int)joystick->axes.size(), (int)joystick->buttons.size());
}

static void JoystickAddedCallback(void* refcon, io_iterator_t iterator)
{
    for(io_object_t ioHIDDeviceObject = IOIteratorNext(iterator); ioHIDDeviceObject; ioHIDDeviceObject = IOIteratorNext(iterator))
    	AddDevice(ioHIDDeviceObject);
}

static void ClearDevices()
{
	for(size_t i = 0; i < g_devices.size(); ++i)
		delete g_devices[i];

	g_devices.resize(0);
}

int JoyInit()
{
	ClearDevices();
	g_devices.reserve(10);

    mach_port_t masterPort = 0;
    IOReturn result = IOMasterPort(bootstrap_port, &masterPort);
    if(result != kIOReturnSuccess)
    {
    	printf("IOMasterPort() failed.\n");
        return 0;
    }

    io_iterator_t hidObjectIterator = 0;
    result = IOServiceGetMatchingServices(masterPort, IOServiceMatching(kIOHIDDeviceKey), &hidObjectIterator);
    if(result != kIOReturnSuccess)
    {
    	printf("IOServiceGetMatchingServices() failed.\n");
    	return 0;
    }

    // List devices.
    if(hidObjectIterator)
    {
	    for(io_object_t ioHIDDeviceObject = IOIteratorNext(hidObjectIterator); ioHIDDeviceObject; ioHIDDeviceObject = IOIteratorNext(hidObjectIterator))
        	AddDevice(ioHIDDeviceObject);

    	IOObjectRelease(hidObjectIterator);
    }

    // Register for notifications when new devices are added.
    g_notificationPort = IONotificationPortCreate(masterPort);
    CFRunLoopAddSource(CFRunLoopGetCurrent(), IONotificationPortGetRunLoopSource(g_notificationPort), kCFRunLoopDefaultMode);

    io_iterator_t portIterator = 0;
    IOServiceAddMatchingNotification(g_notificationPort, kIOFirstMatchNotification, IOServiceMatching(kIOHIDDeviceKey), JoystickAddedCallback, NULL, &portIterator);

    // Apparently we need to go to the end with this iterator, otherwise stuff doesn't work.
    while(IOIteratorNext(portIterator)) {}
    return 1;
}

void JoyShutdown()
{
	ClearDevices();

    if(g_notificationPort)
    {
        IONotificationPortDestroy(g_notificationPort);
        g_notificationPort = NULL;
    }
}

int JoyRefreshDevices()
{
	int i = 0;
	while(i < (int)g_devices.size())
	{
		if(!g_devices[i]->removed)
		{
			++i;
			continue;
		}

		// Hopefully we don't have joysticks in use when refresh is called, otherwise
		// this might crash if the active joystick was removed.
		delete g_devices[i];
		int newNumDev = g_devices.size() - 1;
		g_devices[i] = g_devices[newNumDev];
		g_devices.resize(newNumDev);
	}

	return 1;
}

int JoyCountDevices()
{
	return (int)g_devices.size();
}

const char* JoyGetDeviceName(int index)
{
	if( (index < 0) || (index >= (int)g_devices.size()) )
		return "";

	return g_devices[index]->name;
}

void* JoyUseDevice(int index)
{
	if( (index < 0) || (index >= (int)g_devices.size()) || (g_devices[index]->removed) )
		return NULL;

	return g_devices[index];
}

void JoyReleaseDevice(void* joystick)
{
}

static int GetElemValue(JoyInfo* joystick, const ElemInfo& elem, bool isButton)
{
	if(!joystick || !joystick->interface || !elem.cookie)
		return 0;

	IOHIDEventStruct hidEvent;
	IOReturn result = (*joystick->interface)->getElementValue(joystick->interface, elem.cookie, &hidEvent);
	if(result != kIOReturnSuccess)
		return 0;

	if(isButton)
		return (hidEvent.value >= 1);

	double invRange = 1.0 / (elem.max - elem.min);
	int val = (int)((hidEvent.value - elem.min) * invRange * 2000.0) - 1000;
	return val;
}

int JoyPollDevice(void* joystick, JoyState& state)
{
	if(!joystick)
		return 0;

	JoyInfo* joy = (JoyInfo*)joystick;
	memset(&state, 0, sizeof(state));

	int maxAxes = sizeof(state.axes) / sizeof(state.axes[0]);
	int numAxes = std::min(maxAxes, (int)joy->axes.size());
	for(int i = 0; i < numAxes; ++i)
		state.axes[i] = GetElemValue(joy, joy->axes[i], false);

	int maxButtons = sizeof(state.buttons) / sizeof(state.buttons[0]);
	int numButtons = std::min(maxButtons, (int)joy->buttons.size());
	for(int i = 0; i < numButtons; ++i)
		state.buttons[i] = GetElemValue(joy, joy->buttons[i], true);

	return 1;
}
