#ifndef API_WRAPPER_H
#define API_WRAPPER_H

struct JoyState
{
	int			axes[10];
	int			buttons[20];
	int			dpad[4];
};

extern "C"
{

DLLEXPORT int			JoyInit();
DLLEXPORT void			JoyShutdown();
DLLEXPORT int			JoyRefreshDevices();
DLLEXPORT int			JoyCountDevices();
DLLEXPORT const char*	JoyGetDeviceName(int index);
DLLEXPORT void*			JoyUseDevice(int index);
DLLEXPORT void			JoyReleaseDevice(void* joystick);
DLLEXPORT int			JoyPollDevice(void* joystick, JoyState& state);

}

#endif
